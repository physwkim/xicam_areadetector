import silx
from silx.gui import qt
from silx.gui.plot.PlotWindow import Plot2D as _Plot2D
from silx.gui.plot.Profile import ProfileToolBar
from silx.utils.weakref import WeakMethodProxy

class Plot2D(_Plot2D):
    """ Customized Plot2D inherited from silx.gui.plot.Plot2D """

    def __init__(self, parent=None, backend=None):
        # List of information to display at the bottom of the plot
        posInfo = [
            ('X', lambda x, y: x),
            ('Y', lambda x, y: y),
            ('Value', WeakMethodProxy(self._getImageValue)),
            ('Dims',  WeakMethodProxy(self._getImageDims)),
        ]

        super(_Plot2D, self).__init__(parent=parent, backend=backend,
                                      resetzoom=True, autoScale=False,
                                      logScale=False, grid=False,
                                      curveStyle=False, colormap=True,
                                      aspectRatio=True, yInverted=True,
                                      copy=False, save=False, print_=False,
                                      control=False, position=posInfo,
                                      roi=False, mask=True)

        self.getXAxis().setLabel('Columns')
        self.getYAxis().setLabel('Rows')

        self.getYAxis().setInverted(True)
        self.colorbarAction.setVisible(True)
        self.getColorBarWidget().setVisible(False)

        # Set default colormap
        colorMap = self.getDefaultColormap()
        colorMap.setNormalization('log')
        colorMap.setName('inferno')

        # Show MaskDockWidget
        self.getMaskToolsDockWidget().show()

        self.sigActiveImageChanged.connect(self.__activeImageChanged)
