""" From Xi-cam.gui/xicam/gui/widgets/dataresourcebrower.py  """

import sys
import os
import webbrowser
import pathlib
from pathlib import Path
from urllib import parse
from functools import partial
import logging

from silx.gui import qt
from silx.gui.qt import QTabWidget, QHBoxLayout, QVBoxLayout, QToolBar, QMenu, QAction, QTreeView, QListView, QWidget, QSizePolicy, QTabBar
from silx.gui.qt import QObject, QAbstractItemModel, QSize, Qt, QEvent, Signal
from silx.gui.qt import QIcon, QPixmap, QKeyEvent

logger = logging.Logger(__name__)

def path(item: str):
    return str(pathlib.Path(pathlib.Path(__file__).parent, item))

class SearchLineEdit(qt.QLineEdit):
    def __init__(self, text="", clearable=True, parent=None):
        qt.QLineEdit.__init__(self, text=text, parent=parent)

        searchPixmap = qt.QPixmap(str(path("icons/search.png")))

        clearPixmap = qt.QPixmap(str(path("icons/clear.png")))
        self.clearButton = qt.QToolButton(self)
        self.clearButton.setIcon(QIcon(clearPixmap))
        self.clearButton.setIconSize(QSize(16, 16))
        self.clearButton.setCursor(Qt.ArrowCursor)
        self.clearButton.setStyleSheet("QToolButton { border: none; padding: 0 px;}")
        self.clearButton.hide()

        if clearable:
            self.clearButton.clicked.connect(self.clear)
            self.textChanged.connect(self.updateCloseButton)

        self.searchButton = qt.QToolButton(self)
        self.searchButton.setIcon(QIcon(searchPixmap))
        self.searchButton.setIconSize(QSize(16, 16))
        self.searchButton.setStyleSheet("QToolButton { border: none; padding: 0 px;}")

        frameWidth = self.style().pixelMetric(qt.QStyle.PM_DefaultFrameWidth)
        self.setStyleSheet(
            "QLineEdit { padding-left: %spx; padding - right: % spx;} "
            % (self.searchButton.sizeHint().width() + frameWidth + 1, self.clearButton.sizeHint().width() + frameWidth + 1)
        )
        msz = self.minimumSizeHint()
        self.setMinimumSize(
            max(msz.width(), self.searchButton.sizeHint().width() + self.clearButton.sizeHint().width() + frameWidth * 2 + 2),
            max(msz.height(), self.clearButton.sizeHint().height() + frameWidth * 2 + 2),
        )

    def resizeEvent(self, event):
        sz = self.clearButton.sizeHint()
        frameWidth = self.style().pixelMetric(qt.QStyle.PM_DefaultFrameWidth)
        self.clearButton.move(self.rect().right() - frameWidth - sz.width(), (self.rect().bottom() + 1 - sz.height()) / 2)
        self.searchButton.move(self.rect().left() + 1, (self.rect().bottom() + 1 - sz.height()) / 2)

    def updateCloseButton(self, text):
        if text:
            self.clearButton.setVisible(True)
        else:
            self.clearButton.setVisible(False)

class BrowserTabWidget(QTabWidget):
    def __init__(self, parent=None):
        super(BrowserTabWidget, self).__init__(parent)
        self.setContentsMargins(0, 0, 0, 0)

class DataBrowser(QWidget):
    sigOpen = Signal(object)
    sigAppend = Signal(object)
    sigPreview = Signal(object)
    sigPathChanged = Signal(str)

    def __init__(self, browserview):
        super(DataBrowser, self).__init__()

        hbox = QHBoxLayout()
        vbox = QVBoxLayout()
        vbox.setContentsMargins(0, 0, 0, 0)
        vbox.setSpacing(0)
        hbox.setContentsMargins(0, 0, 0, 0)
        hbox.setSpacing(0)
        self.setContentsMargins(0, 0, 0, 0)

        self.browserview = browserview
        self.browserview.sigOpen.connect(self.sigOpen)
        self.browserview.sigAppend.connect(self.sigAppend)
        self.browserview.sigPreview.connect(self.sigPreview)
        self.browserview.sigOpenExternally.connect(self.openExternally)
        self.browserview.sigURIChanged.connect(self.uri_to_text)
        self.toolbar = QToolBar()
        self.toolbar.addAction(QIcon(QPixmap(str(path("icons/up.png")))), "Move up directory", self.moveUp)
        self.toolbar.addAction(QIcon(QPixmap(str(path("icons/refresh.png")))), "Refresh", self.hardRefreshURI)
        self.toolbar.setToolButtonStyle(Qt.ToolButtonIconOnly)
        self.URILineEdit = SearchLineEdit("", clearable=False)
        self.uri_to_text()

        hbox.addWidget(self.toolbar)
        hbox.addWidget(self.URILineEdit)
        vbox.addLayout(hbox)
        vbox.addWidget(self.browserview)
        self.setLayout(vbox)

        self.URILineEdit.textChanged.connect(self.softRefreshURI)
        self.URILineEdit.returnPressed.connect(self.softRefreshURI)  # hard refresh
        self.URILineEdit.focusOutEvent = self.softRefreshURI  # hard refresh

        self.hardRefreshURI()

    def printFiles(self, selections):
        print("selections  = ", selections)

    def text_to_uri(self):
        uri = parse.urlparse(self.URILineEdit.text())
        self.browserview.model().uri = uri
        return uri

    def setPath(self, path):
        self.URILineEdit.setText(str(path))

    def uri_to_text(self):
        uri = self.browserview.model().uri
        text = parse.urlunparse(uri)
        self.URILineEdit.setText(text)
        self.sigPathChanged.emit(text)
        return text

    def hardRefreshURI(self, *_, **__):
        self.text_to_uri()
        self.browserview.refresh()

    def moveUp(self):
        self.browserview.model().uri = parse.urlparse(str(Path(self.URILineEdit.text()).parent))
        self.browserview.refresh()
        self.uri_to_text()

    def openExternally(self, uri):
        webbrowser.open(uri)

    softRefreshURI = hardRefreshURI

class DataResourceView(QObject):
    def __init__(self, model: QAbstractItemModel):
        super(DataResourceView, self).__init__()
        self._model = model  # type: QAbstractItemModel
        self.setModel(self._model)
        self.doubleClicked.connect(self.open)
        self.setSelectionMode(self.ExtendedSelection)
        self.setSelectionBehavior(self.SelectRows)
        self.setContextMenuPolicy(Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.menuRequested)

        self.menu = QMenu()
        standardActions = [
            QAction("Open", self),
            QAction("Append", self),
            QAction("Delete", self),
        ]
        self.menu.addActions(standardActions)
        standardActions[0].triggered.connect(self.open)
        standardActions[1].triggered.connect(self.append)
        standardActions[2].triggered.connect(self.delete)
        # standardActions[0].triggered.connect(self.reduce)
        # standardActions[1].triggered.connect(self.openExternally)

    def menuRequested(self, position):
        self.menu.exec_(self.viewport().mapToGlobal(position))

    def open(self, index):
        pass

    def append(self, index):
        pass

    def reduce(self, index):
        pass

    def currentChanged(self, current, previous):
        pass

    def openExternally(self, uri: str):
        pass


class DataResourceTree(QTreeView, DataResourceView):
    sigOpen = Signal(object)
    sigAppend = Signal(object)
    sigOpenPath = Signal(str)
    sigOpenExternally = Signal(str)
    sigPreview = Signal(object)
    sigURIChanged = Signal(str)

    def __init__(self, *args):
        super(DataResourceTree, self).__init__(*args)

    def refresh(self):
        self.model().refresh()
        self.setRootIndex(self.model().index(self.model().path))

class LocalFileSystemTree(DataResourceTree):
    def __init__(self):
        super(LocalFileSystemTree, self).__init__(LocalFileSystemResourcePlugin())

    def open(self, _=None):
        indexes = self.selectionModel().selectedRows()
        if len(indexes) == 1:
            path = self.model().filePath(indexes[0])
            if os.path.isdir(path):
                self.model().path = path
                self.setRootIndex(indexes[0])
                self.sigURIChanged.emit(path)
                return
        self.sigOpen.emit(self.model().getHeader(indexes))

    def append(self, _=None):
        indexes = self.selectionModel().selectedRows()
        if len(indexes) == 1:
            path = self.model().filePath(indexes[0])
            if os.path.isdir(path):
                self.model().path = path
                self.setRootIndex(indexes[0])
                self.sigURIChanged.emit(path)
                return
        self.sigAppend.emit(self.model().getHeader(indexes))

    def delete(self, _=None):
        indexes = self.selectionModel().selectedRows()
        for index in indexes:
            path = self.model().filePath(index)
            if os.path.isfile(path):
                try:
                    os.remove(path)
                    logger.debug(path + 'is deleted')
                except:
                    logger.warning("Could not delete a file!")
                    pass

    def currentChanged(self, current, previous):
        if current.isValid():
            self.sigPreview.emit(self.model().getHeader([current]))

        self.scrollTo(current)

    def keyPressEvent(self, event: QKeyEvent):
        super(LocalFileSystemTree, self).keyPressEvent(event)
        if event.key() in [Qt.Key_Enter, Qt.Key_Return]:
            event.accept()
            self.open()

    def openExternally(self, uri: str):
        indexes = self.selectionModel().selectedRows()
        for index in indexes:
            self.sigOpenExternally.emit(self.model().filePath(index))

class LocalFileSystemResourcePlugin(qt.QFileSystemModel):
    def __init__(self):
        super(LocalFileSystemResourcePlugin, self).__init__()

        self.uri = parse.urlparse(qt.QSettings().value("lastlocaldir", os.getcwd()))
        self.setResolveSymlinks(True)

        self.setRootPath(parse.urlunparse(self.uri))

    def setRootPath(self, path):
        if os.path.isdir(path):
            filter = "*"
            root = path
        else:
            filter = os.path.basename(path)
            root = path[: -len(filter)]

        root = qt.QDir(root)
        super(LocalFileSystemResourcePlugin, self).setRootPath(root.absolutePath())
        self.setNameFilters([filter])
        self.uri = parse.urlparse(path)
        qt.QSettings().setValue("lastlocaldir", path)

    @property
    def path(self):
        return self.rootPath()

    @path.setter
    def path(self, value):
        self.setRootPath(value)

    def refresh(self):
        self.setRootPath(parse.urlunparse(self.uri))

    def getHeader(self, indexes):
        uris = [self.filePath(index) for index in indexes]
        return uris

class DataFileResourcePlugin(LocalFileSystemResourcePlugin):
    def __init__(self):
        super(DataFileResourcePlugin, self).__init__()

    def setRootPath(self, path):
        if os.path.isdir(path):
            filter = "*.dat"
            root = path
        else:
            filter = "*.dat"
            root = os.path.basename(path)

        root = qt.QDir(root)
        super(DataFileResourcePlugin, self).setRootPath(root.absolutePath())
        self.setNameFilters([filter])
        self.uri = parse.urlparse(path)
        qt.QSettings().setValue("lastfiledir", path)

class DataFileTree(LocalFileSystemTree):
    def __init__(self):
        super(LocalFileSystemTree, self).__init__(DataFileResourcePlugin())

if __name__ == '__main__':
    app = qt.QApplication(sys.argv)
    main = DataBrowser(LocalFileSystemTree())
    main.show()
    sys.exit(app.exec_())
