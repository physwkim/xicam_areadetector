import os
import logging
import subprocess

from silx.gui import qt
import pyqtgraph.parametertree.parameterTypes as pTypes
from pyqtgraph.parametertree import ParameterTree

from xicam.core import threads
from xicam.areaDetector.pathParameter import FilePathParameter, \
                                             DirPathParameter, \
                                             LabelParameter, \
                                             SimpleParameter
from pyFAI.io.ponifile import PoniFile

logger = logging.getLogger(__name__)

class settingsPanel(ParameterTree):
    sigIntegrate = qt.pyqtSignal()

    def __init__(self, parent, *args, **kwargs):
        super(settingsPanel, self).__init__(*args, **kwargs)

        # pyFAI-calib2 subprocess threads
        self.calib2Thread = None
        self.parent = parent

        # Emit sigIntegrate signal
        self.integrateAction = pTypes.ActionParameter(name='Integrate')
        self.integrateAction.sigActivated.connect(self.sigIntegrate)

        # Checkbox to auto integrate
        self.autoIntegrateCB = SimpleParameter(name='Auto Integrate',
                                               type='bool',
                                               value=False)

        # Open pyFAI-calib2
        self.calibAction = pTypes.ActionParameter(name='Open pyFAI-calib2')
        self.calibAction.sigActivated.connect(self.openPyFAICalib2)


        self.calibPoniFile = FilePathParameter(name='PONI File',
                                               type='str',
                                               dir=False,
                                               fileType='poni (*.poni)')

        # load poni file
        self.calibPoniFile.sigValueChanged.connect(self.openCalibFile)

        self.calibFit2DFile = FilePathParameter(name='Fit2D File',
                                                type='str',
                                                dir=False,
                                                fileType='fit2d (*.fit2d)')
        # Hide as default
        self.calibFit2DFile.show(False)

        # Calibration Type {pyFAI(PONI) or Fit2D}
        self.calibType = pTypes.ListParameter(name='Type',
                                              type='list',
                                              values=['PONI'],
                                              value='PONI')
        self.calibType.sigValueChanged.connect(self.calibTypeToggle)

        # Parameters associated with the experiment
        self.pvParams = LabelParameter(name='Areadetector Parameters',
                                       type='str',
                                       readonly=True,
                                       value='')

        # Checkbox to update from areaDetector PV
        self.prefixPV = SimpleParameter(name='Prefix',
                                        type='str',
                                        value='BL1C:2M:')

        # Checkbox to update from areaDetector PV
        self.autoUpdateCB = SimpleParameter(name='Auto Update',
                                            type='bool',
                                            value=True)

        self.pvParams.addChild(self.autoUpdateCB)
        self.pvParams.addChild(self.prefixPV)

        self.calibParams = LabelParameter(name='Calibration',
                                          type='str',
                                          readonly=True,
                                          value='')

        self.calibParams.addChild(self.calibType)
        self.calibParams.addChild(self.calibPoniFile)
        self.calibParams.addChild(self.calibFit2DFile)
        self.calibParams.addChild(self.calibAction)

        # Parameters associated with the experiment
        self.expParams = LabelParameter(name='Experiment',
                                        type='str',
                                        readonly=True,
                                        value='')

        # Wavelength in meter
        self.wavelength = SimpleParameter(name='Wavelength',
                                          type='float',
                                          decimals=10,
                                          units='m',
                                          value=11.0)

        self.detector = SimpleParameter(name='Detector',
                                        type='str',
                                        value='')

        # pyFAI related parameters
        self.distance = SimpleParameter(name='Distance',
                                        type='float',
                                        decimals=10,
                                        units='m',
                                        value=0.0)

        self.poni1 = SimpleParameter(name='Poni1',
                                     type='float',
                                     decimals=10,
                                     units='m',
                                     value=0.0)

        self.poni2 = SimpleParameter(name='Poni2',
                                     type='float',
                                     decimals=10,
                                     units='m',
                                     value=0.0)

        self.rot1 = SimpleParameter(name='Rot1',
                                    type='float',
                                    decimals=10,
                                    units='deg',
                                    value=0.0)

        self.rot2 = SimpleParameter(name='Rot2',
                                    type='float',
                                    decimals=10,
                                    units='deg',
                                    value=0.0)

        self.rot3 = SimpleParameter(name='Rot3',
                                    type='float',
                                    decimals=10,
                                    units='deg',
                                    value=0.0)

        # Fit2D related parameters (centerX, centerY, tilt, tiltPlan)
        self.centerX = SimpleParameter(name='centerX',
                                       type='float',
                                       decimals=10,
                                       units='m',
                                       value=0.0)

        self.centerY = SimpleParameter(name='centerY',
                                       type='float',
                                       decimals=10,
                                       units='m',
                                       value=0.0)

        self.tilt = SimpleParameter(name='tilt',
                                    type='float',
                                    decimals=10,
                                    units='deg',
                                    value=0.0)

        self.tiltPlan = SimpleParameter(name='tiltPlan',
                                        type='float',
                                        decimals=10,
                                        units='deg',
                                        value=0.0)

        # pyFAI related parameters
        self.expParams.addChild(self.detector)
        self.expParams.addChild(self.wavelength)
        self.expParams.addChild(self.distance)
        self.expParams.addChild(self.poni1)
        self.expParams.addChild(self.poni2)
        self.expParams.addChild(self.rot1)
        self.expParams.addChild(self.rot2)
        self.expParams.addChild(self.rot3)

        # Fit2D related parameters
        self.expParams.addChild(self.centerX)
        self.expParams.addChild(self.centerY)
        self.expParams.addChild(self.tilt)
        self.expParams.addChild(self.tiltPlan)

        # Hide Fit2D related parameters as default
        self.centerX.show(False)
        self.centerY.show(False)
        self.tilt.show(False)
        self.tiltPlan.show(False)

        # Parameters associated with the Background
        self.backgroundParams = LabelParameter(name='Background',
                                               type='str',
                                               readonly=True,
                                               value='')

        # Background file path
        self.bgPath = FilePathParameter(name='Background File Path',
                                        type='str',
                                        value='',
                                        dir=True,
                                        fileType='Header (*.tif *.edf)')
        self.bgPath.show(False)


        # Checkbox to update from areaDetector PV
        self.bgCB = SimpleParameter(name='Subtract',
                                    type='bool',
                                    value=False)
        self.bgCB.sigValueChanged.connect(self.bgPathToggle)

        self.backgroundParams.addChild(self.bgCB)
        self.backgroundParams.addChild(self.bgPath)

        # Parameters associated with the reduction
        self.integrateParams = LabelParameter(name='Integration',
                                              type='str',
                                              readonly=True,
                                              value='')

        # Reduce method
        self.method = pTypes.ListParameter(name='Reduce Method',
                                           type='list',
                                           values=['numpy',
                                                   'cython',
                                                   'BBox',
                                                   'splitpixel',
                                                   'lut',
                                                   'csr',
                                                   'nosplit_csr',
                                                   'full_csr',
                                                   'lut_ocl',
                                                   'csr_ocl'],
                                           value='splitpixel')
        # Mask file
        self.mask = FilePathParameter(name='Mask File',
                                      type='str',
                                      dir=False,
                                      fileType='image (*.edf *.tif *.tiff)')

        # spline file
        self.spline = FilePathParameter(name='Spline File',
                                        type='str',
                                        dir=False,
                                        fileType='spline (*.spline)')

        # Polarization factor
        self.polCB = SimpleParameter(name='Polarization',
                                     type='bool',
                                     value=False)

        self.polz_factor = SimpleParameter(name='Polarization Factor',
                                           type='float',
                                           value=0.95)
        self.polz_factor.show(False)

        self.polCB.addChild(self.polz_factor)
        self.polCB.sigValueChanged.connect(self.polFactorToggle)


        self.radialUnit = pTypes.ListParameter(name='Radial Unit',
                                               type='list',
                                               values=['2th_deg',
                                                       '2th_rad',
                                                       'q_nm^-1',
                                                       'q_A^-1'],
                                               value='2th_deg')

        self.radialPoints = SimpleParameter(name='Radial Points',
                                            type='int',
                                            decimals=5,
                                            value=1024)

        self.azimuthalPoints = SimpleParameter(name='Azimuthal Points',
                                               type='int',
                                               decimals=5,
                                               value=360)

        self.integrateParams.addChild(self.method)
        self.integrateParams.addChild(self.mask)
        self.integrateParams.addChild(self.spline)
        self.integrateParams.addChild(self.polCB)
        self.integrateParams.addChild(self.radialUnit)
        self.integrateParams.addChild(self.radialPoints)
        self.integrateParams.addChild(self.azimuthalPoints)
        self.integrateParams.addChild(self.autoIntegrateCB)
        self.integrateParams.addChild(self.integrateAction)

        self.parameter = pTypes.GroupParameter(name='settings',
                                               children=[self.pvParams,
                                                         self.backgroundParams,
                                                         self.calibParams,
                                                         self.integrateParams,
                                                         self.expParams])

        self.setParameters(self.parameter, showTop=False)

    def openCalibFile(self, param):
        """ Open calibration file and load parameters """
        calibFile = param.value()

        # Check file exists
        try:
            if not os.path.isfile(calibFile):
                return
        except:
            logger.warning("Calibration file is not valid")
            return

        calibType = self.calibType.value()

        if calibType == 'PONI':
            try:
                self.calibFile = PoniFile(calibFile)
                config = self.calibFile.as_dict()

                threads.invoke_in_main_thread(self.detector.setValue,   str(config['detector']))
                threads.invoke_in_main_thread(self.wavelength.setValue, float(config['wavelength']))
                threads.invoke_in_main_thread(self.distance.setValue,   float(config['dist']))
                threads.invoke_in_main_thread(self.poni1.setValue,      float(config['poni1']))
                threads.invoke_in_main_thread(self.poni2.setValue,      float(config['poni2']))
                threads.invoke_in_main_thread(self.rot1.setValue,       float(config['rot1']))
                threads.invoke_in_main_thread(self.rot2.setValue,       float(config['rot2']))
                threads.invoke_in_main_thread(self.rot3.setValue,       float(config['rot3']))
            except:
                logger.warning("Poni file is not valid. Couldn't update configuration")
                return

    def calibTypeToggle(self, param):
        """ Toggle PONI and Fit2D parameters """
        if param.value() == 'PONI':
            # pyFAI related parameters
            self.detector.show(True)
            self.distance.show(True)
            self.poni1.show(True)
            self.poni2.show(True)
            self.rot1.show(True)
            self.rot2.show(True)
            self.rot3.show(True)

            # Fit2D related parameters
            self.centerX.show(False)
            self.centerY.show(False)
            self.tilt.show(False)
            self.tiltPlan.show(False)

            self.calibPoniFile.show(True)
            self.calibFit2DFile.show(False)

        else:
            # pyFAI related parameters
            self.detector.show(False)
            self.distance.show(False)
            self.poni1.show(False)
            self.poni2.show(False)
            self.rot1.show(False)
            self.rot2.show(False)
            self.rot3.show(False)

            # Fit2D related parameters
            self.centerX.show(True)
            self.centerY.show(True)
            self.tilt.show(True)
            self.tiltPlan.show(True)

            self.calibPoniFile.show(False)
            self.calibFit2DFile.show(True)

    def polFactorToggle(self, param):
        self.polz_factor.show(param.value())

    def bgPathToggle(self, param):
        self.bgPath.show(param.value())

    def openPyFAICalib2(self):
        """ Open pyFAI-calib2 in subprocess"""

        if self.calib2Thread:
            if self.calib2Thread.running:
                logger.warning("thread already running!")
                return

        def calib2():
            command = ['pyFAI-calib2']
            self.calib2Proc = subprocess.Popen(command)
            self.calib2Proc.wait()

        self.calib2Thread = threads.QThreadFuture(calib2, threadkey='calib2', showBusy=False)
        self.calib2Thread.start()

    def closePyFAICalib2(self):
        """ Close pyFAI-calib2 in subprocess"""

        if self.calib2Thread:
            if self.calib2Thread.running:
                logger.warning("close thread!")
                self.calib2Proc.kill()
                return
