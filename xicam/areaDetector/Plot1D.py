from silx.gui import qt
from silx.gui.plot.PlotWindow import Plot1D as _Plot1D

class Plot1D(_Plot1D):
    """Customized Plot1D inherited from silx.gui.plot.PlotWindow"""

    def __init__(self, *args, **kwargs):
        super(Plot1D, self).__init__(*args, **kwargs)
