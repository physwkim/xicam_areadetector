import numpy

from silx.gui import qt
from silx.gui.plot import actions
from silx.gui.plot import PlotWidget
from silx.gui.plot import tools
from silx.gui.plot import PlotToolButtons
from silx.gui.plot.actions import control as actions_control
from silx.gui.plot.Profile import ProfileToolBar
from silx.gui.plot.ImageView import ImageView as _ImageView
from silx.gui.plot.actions.control import ZoomBackAction

from silx.utils.weakref import WeakMethodProxy

class RadarView(qt.QGraphicsView):
    """Widget presenting a synthetic view of a 2D area and
    the current visible area.

    Coordinates are as in QGraphicsView:
    x goes from left to right and y goes from top to bottom.
    This widget preserves the aspect ratio of the areas.

    The 2D area and the visible area can be set with :meth:`setDataRect`
    and :meth:`setVisibleRect`.
    When the visible area has been dragged by the user, its new position
    is signaled by the *visibleRectDragged* signal.

    It is possible to invert the direction of the axes by using the
    :meth:`scale` method of QGraphicsView.
    """

    visibleRectDragged = qt.Signal(float, float, float, float)
    """Signals that the visible rectangle has been dragged.

    It provides: left, top, width, height in data coordinates.
    """

    _DATA_PEN = qt.QPen(qt.QColor('white'))
    _DATA_BRUSH = qt.QBrush(qt.QColor('light gray'))
    _VISIBLE_PEN = qt.QPen(qt.QColor('red'))
    _VISIBLE_PEN.setWidth(2)
    _VISIBLE_PEN.setCosmetic(True)
    _VISIBLE_BRUSH = qt.QBrush(qt.QColor(0, 0, 0, 0))
    _TOOLTIP = 'Radar View:\nRed contour: Visible area\nGray area: The image'

    _PIXMAP_SIZE = 256

    class _NoDraggableRectItem(qt.QGraphicsRectItem):
        """RectItem which signals its change through visibleRectDragged."""
        def __init__(self, *args, **kwargs):
            super(RadarView._NoDraggableRectItem, self).__init__(
                *args, **kwargs)

            self._previousCursor = None
            # self.setFlag(qt.QGraphicsItem.ItemIsMovable)
            self.setFlag(qt.QGraphicsItem.ItemSendsGeometryChanges)
            self.setAcceptHoverEvents(True)
            self._ignoreChange = True
            self._constraint = 0, 0, 0, 0

        def setConstraintRect(self, left, top, width, height):
            """Set the constraint rectangle for dragging.

            The coordinates are in the _NoDraggableRectItem coordinate system.

            This constraint only applies to modification through interaction
            (i.e., this constraint is not applied to change through API).

            If the _NoDraggableRectItem is smaller than the constraint rectangle,
            the _NoDraggableRectItem remains within the constraint rectangle.
            If the _NoDraggableRectItem is wider than the constraint rectangle,
            the constraint rectangle remains within the _NoDraggableRectItem.
            """
            self._constraint = left, left + width, top, top + height

        def setPos(self, *args, **kwargs):
            """Overridden to ignore changes from API in itemChange."""
            self._ignoreChange = True
            super(RadarView._NoDraggableRectItem, self).setPos(*args, **kwargs)
            self._ignoreChange = False

        def moveBy(self, *args, **kwargs):
            """Overridden to ignore changes from API in itemChange."""
            self._ignoreChange = True
            super(RadarView._NoDraggableRectItem, self).moveBy(*args, **kwargs)
            self._ignoreChange = False

        def itemChange(self, change, value):
            """Callback called before applying changes to the item."""
            if (change == qt.QGraphicsItem.ItemPositionChange and
                    not self._ignoreChange):
                # Makes sure that the visible area is in the data
                # or that data is in the visible area if area is too wide
                x, y = value.x(), value.y()
                xMin, xMax, yMin, yMax = self._constraint

                if self.rect().width() <= (xMax - xMin):
                    if x < xMin:
                        value.setX(xMin)
                    elif x > xMax - self.rect().width():
                        value.setX(xMax - self.rect().width())
                else:
                    if x > xMin:
                        value.setX(xMin)
                    elif x < xMax - self.rect().width():
                        value.setX(xMax - self.rect().width())

                if self.rect().height() <= (yMax - yMin):
                    if y < yMin:
                        value.setY(yMin)
                    elif y > yMax - self.rect().height():
                        value.setY(yMax - self.rect().height())
                else:
                    if y > yMin:
                        value.setY(yMin)
                    elif y < yMax - self.rect().height():
                        value.setY(yMax - self.rect().height())

                if self.pos() != value:
                    # Notify change through signal
                    views = self.scene().views()
                    assert len(views) == 1
                    views[0].visibleRectDragged.emit(
                        value.x() + self.rect().left(),
                        value.y() + self.rect().top(),
                        self.rect().width(),
                        self.rect().height())

                return value

            return super(RadarView._NoDraggableRectItem, self).itemChange(
                change, value)

        def hoverEnterEvent(self, event):
            """Called when the mouse enters the rectangle area"""
            self._previousCursor = self.cursor()
            self.setCursor(qt.Qt.OpenHandCursor)

        def hoverLeaveEvent(self, event):
            """Called when the mouse leaves the rectangle area"""
            if self._previousCursor is not None:
                self.setCursor(self._previousCursor)
                self._previousCursor = None

    def __init__(self, parent=None):
        self._scene = qt.QGraphicsScene()
        self._dataRect = self._scene.addRect(0, 0, 1, 1,
                                             self._DATA_PEN,
                                             self._DATA_BRUSH)
        self._visibleRect = self._NoDraggableRectItem(0, 0, 1, 1)
        self._visibleRect.setPen(self._VISIBLE_PEN)
        self._visibleRect.setBrush(self._VISIBLE_BRUSH)
        self._scene.addItem(self._visibleRect)

        super(RadarView, self).__init__(self._scene, parent)
        self.setHorizontalScrollBarPolicy(qt.Qt.ScrollBarAlwaysOff)
        self.setVerticalScrollBarPolicy(qt.Qt.ScrollBarAlwaysOff)
        self.setFocusPolicy(qt.Qt.NoFocus)
        self.setStyleSheet('border: 0px')
        self.setToolTip(self._TOOLTIP)

    def sizeHint(self):
        # """Overridden to avoid sizeHint to depend on content size."""
        return self.minimumSizeHint()

    def wheelEvent(self, event):
        # """Overridden to disable vertical scrolling with wheel."""
        event.ignore()

    def resizeEvent(self, event):
        # """Overridden to fit current content to new size."""
        self.fitInView(self._scene.itemsBoundingRect(), qt.Qt.KeepAspectRatio)
        super(RadarView, self).resizeEvent(event)

    def setDataRect(self, left, top, width, height):
        """Set the bounds of the data rectangular area.

        This sets the coordinate system.
        """
        self._dataRect.setRect(left, top, width, height)
        self._visibleRect.setConstraintRect(left, top, width, height)
        self.fitInView(self._scene.itemsBoundingRect(), qt.Qt.KeepAspectRatio)

    def setVisibleRect(self, left, top, width, height):
        """Set the visible rectangular area.

        The coordinates are relative to the data rect.
        """
        self._visibleRect.setRect(0, 0, width, height)
        self._visibleRect.setPos(left, top)
        self.fitInView(self._scene.itemsBoundingRect(), qt.Qt.KeepAspectRatio)


class ImageView(_ImageView):
    '''
    Customized ImageView class inherited from silx.gui.plot.ImageView

    '''
    def __init__(self, parent=None, backend=None, *args, **kwargs):
        self.parent = parent
        self._dataInfo = None
        self._imageLegend = '__ImageView__image' + str(id(self))
        self._cache = None  # Store currently visible data information
        self._updatingLimits = False

        posInfo = [
            ('X', lambda x, y: x),
            ('Y', lambda x, y: y),
            ('Value', WeakMethodProxy(self._getImageValue)),
            ('Dims', WeakMethodProxy(self._getImageDims)),
        ]

        # super(ImageView, self).__init__(*args, **kwargs)
        super(_ImageView, self).__init__(parent=parent, backend=backend,
                                         resetzoom=True, autoScale=False,
                                         logScale=False, grid=False,
                                         curveStyle=False, colormap=True,
                                         aspectRatio=True, yInverted=True,
                                         copy=True, save=True, print_=True,
                                         control=False, position=posInfo,
                                         roi=False, mask=True)

        self.getXAxis().setLabel('')
        self.getYAxis().setLabel('')
        self.setGraphTitle('Image')

        colorMap = self.getColormap()
        colorMap.setNormalization('log')
        colorMap.setName('inferno')

        if parent is None:
            self.setWindowTitle('ImageView')

        self.profile = ProfileToolBar(plot=self)
        self.addToolBar(self.profile)

        # Enable Colorbar control action
        self.colorbarAction.setVisible(True)

        # Set Y Axis Inverted
        self.setYAxisInverted(True)

        # Enable cursor(red)
        # self.setGraphCursor(True, color='red')

        """
        menu = self.menuBar().addMenu('File')
        menu.addAction(self.getOutputToolBar().getSaveAction())
        menu.addAction(self.getOutputToolBar().getPrintAction())
        menu.addSeparator()
        action = menu.addAction('Quit')
        action.triggered[bool].connect(qt.QApplication.instance().quit)

        menu = self.menuBar().addMenu('Edit')
        menu.addAction(self.getOutputToolBar().getCopyAction())
        menu.addSeparator()
        menu.addAction(self.getResetZoomAction())
        menu.addAction(self.getColormapAction())
        menu.addAction(actions.control.KeepAspectRatioAction(self, self))
        menu.addAction(actions.control.YAxisInvertedAction(self, self))
        """

        # To change color from red to black in _positionWidget
        self._positionWidget.setSnappingMode(0)

        # Connect to ImageView's signal
        self.valueChanged.connect(self._cursorSlot)

        # ZoomBack Action
        self._zoomBackAction = ZoomBackAction(plot=self, parent=self)

        # Retrieve PlotWidget's plot area widget
        plotArea = self.getWidgetHandle()

        # Set plot area custom context menu
        plotArea.setContextMenuPolicy(qt.Qt.CustomContextMenu)
        plotArea.customContextMenuRequested.connect(self._contextMenu)

    def _contextMenu(self, pos):
        """Handle plot area customContextMenuRequested signal.

        :param QPoint pos: Mouse position relative to plot area
        """
        # Create the context menu
        menu = qt.QMenu(self)
        menu.addAction(self._zoomBackAction)

        # Displaying the context menu at the mouse position requires
        # a global position.
        # The position received as argument is relative to PlotWidget's
        # plot area, and thus needs to be converted.
        plotArea = self.getWidgetHandle()
        globalPosition = plotArea.mapToGlobal(pos)
        menu.exec_(globalPosition)

    def _updateRadarView(self):
        """Update radar view visible area.

        Takes care of y coordinate conversion.
        """
        xMin, xMax = self.getXAxis().getLimits()
        yMin, yMax = self.getYAxis().getLimits()
        self.parent._radarView.setVisibleRect(xMin, yMin, xMax - xMin, yMax - yMin)

    def _updateYAxisInverted(self, inverted=None):
        """Sync image, vertical histogram and radar view axis orientation."""
        if inverted is None:
            # Do not perform this when called from plot signal
            inverted = self.getYAxis().isInverted()

        self.parent._histoVPlot.getYAxis().setInverted(inverted)
        image = self.getImage()
        if image:
            self.setImage(image.getData(copy=False))

        # Use scale to invert radarView
        # RadarView default Y direction is from top to bottom
        # As opposed to Plot. So invert RadarView when Plot is NOT inverted.
        self.parent._radarView.resetTransform()
        if not inverted:
            self.parent._radarView.scale(1., -1.)
        self._updateRadarView()

        self.parent._radarView.update()

    def _cursorSlot(self, column, row, value):
        if numpy.isnan(row):
            return
        elif numpy.isnan(column):
            return
        else:
            msg = 'Position: (%d, %d), Value: %g' % (int(row), int(column),
                                                     value)
            # print(msg)

        self._updateProfiles(int(row), int(column))

    def setImage(self, image, origin=(0, 0), scale=(1., 1.),
                 copy=True, reset=True):
        """Set the image to display.

        :param image: A 2D array representing the image or None to empty plot.
        :type image: numpy.ndarray-like with 2 dimensions or None.
        :param origin: The (x, y) position of the origin of the image.
                       Default: (0, 0).
                       The origin is the lower left corner of the image when
                       the Y axis is not inverted.
        :type origin: Tuple of 2 floats: (origin x, origin y).
        :param scale: The scale factor to apply to the image on X and Y axes.
                      Default: (1, 1).
                      It is the size of a pixel in the coordinates of the axes.
                      Scales must be positive numbers.
        :type scale: Tuple of 2 floats: (scale x, scale y).
        :param bool copy: Whether to copy image data (default) or not.
        :param bool reset: Whether to reset zoom and ROI (default) or not.
        """
        self._dirtyCache()

        assert len(origin) == 2
        assert len(scale) == 2
        assert scale[0] > 0
        assert scale[1] > 0

        if image is None:
            self.remove(self._imageLegend, kind='image')
            return

        data = numpy.array(image, order='C', copy=copy)
        assert data.size != 0
        assert len(data.shape) == 2
        height, width = data.shape

        self.addImage(data,
                      legend=self._imageLegend,
                      origin=origin, scale=scale,
                      colormap=self.getColormap(),
                      resetzoom=False)
        self.setActiveImage(self._imageLegend)

        self.parent._radarView.setDataRect(origin[0],
                                    origin[1],
                                    width * scale[0],
                                    height * scale[1])

        if reset:
            self.resetZoom()
        else:
            self._updateHistogramsLimits()

        self.setGraphCursor(True, color='red')

    def _updateProfiles(self, row, column):
        """Update profile curves under the mouse cursor using current active image."""
        activeImage = self.getActiveImage()
        if activeImage is not None:
            wasUpdatingLimits = self._updatingLimits
            self._updatingLimits = True

            data = activeImage.getData(copy=False)
            origin = activeImage.getOrigin()
            scale = activeImage.getScale()
            height, width = data.shape

            xMin, xMax = self.getXAxis().getLimits()
            yMin, yMax = self.getYAxis().getLimits()

            # Convert plot area limits to image coordinates
            # and work in image coordinates (i.e., in pixels)
            xMin = int((xMin - origin[0]) / scale[0])
            xMax = int((xMax - origin[0]) / scale[0])
            yMin = int((yMin - origin[1]) / scale[1])
            yMax = int((yMax - origin[1]) / scale[1])

            if (xMin < width and xMax >= 0 and
                    yMin < height and yMax >= 0):
                # The image is at least partly in the plot area
                # Get the visible bounds in image coords (i.e., in pixels)
                subsetXMin = 0 if xMin < 0 else xMin
                subsetXMax = (width if xMax >= width else xMax) + 1
                subsetYMin = 0 if yMin < 0 else yMin
                subsetYMax = (height if yMax >= height else yMax) + 1

                histoHVisibleData = data[row, :][subsetXMin:subsetXMax]
                histoVVisibleData = data[:, column][subsetYMin:subsetYMax]

                self._cache = {
                    'dataXMin': subsetXMin,
                    'dataXMax': subsetXMax,
                    'dataYMin': subsetYMin,
                    'dataYMax': subsetYMax,

                    'histoH': histoHVisibleData,
                    'histoHMin': numpy.min(histoHVisibleData),
                    'histoHMax': numpy.max(histoHVisibleData),

                    'histoV': histoVVisibleData,
                    'histoVMin': numpy.min(histoVVisibleData),
                    'histoVMax': numpy.max(histoVVisibleData)
                }

                # Convert to histogram curve and update plots
                # Taking into account origin and scale
                coords = numpy.arange(histoHVisibleData.size)
                xCoords = coords + subsetXMin
                xCoords = origin[0] + scale[0] * xCoords
                xData = histoHVisibleData
                self.parent._histoHPlot.addCurve(xCoords, xData,
                                          xlabel='', ylabel='',
                                          replace=False,
                                          color=self.HISTOGRAMS_COLOR,
                                          linestyle='-',
                                          selectable=False)
                vMin = self._cache['histoHMin']
                vMax = self._cache['histoHMax']
                vOffset = 0.1 * (vMax - vMin)
                if vOffset == 0.:
                    vOffset = 1.
                self.parent._histoHPlot.getYAxis().setLimits(vMin - vOffset,
                                                      vMax + vOffset)

                coords = numpy.arange(histoVVisibleData.size)
                yCoords = coords + subsetYMin
                yCoords = origin[1] + scale[1] * yCoords
                yData = histoVVisibleData
                self.parent._histoVPlot.addCurve(yData, yCoords,
                                          xlabel='', ylabel='',
                                          replace=False,
                                          color=self.HISTOGRAMS_COLOR,
                                          linestyle='-',
                                          selectable=False)
                vMin = self._cache['histoVMin']
                vMax = self._cache['histoVMax']
                vOffset = 0.1 * (vMax - vMin)
                if vOffset == 0.:
                    vOffset = 1.
                self.parent._histoVPlot.getXAxis().setLimits(vMin - vOffset,
                                                      vMax + vOffset)

            self._updatingLimits = wasUpdatingLimits

    def _updateHistogramsLimits(self):
            # Do not handle histograms limitsChanged while
            # updating their limits from here.
            self._updatingLimits = True

            # Refresh histograms
            # self._updateHistograms()

            xMin, xMax = self.getXAxis().getLimits()
            yMin, yMax = self.getYAxis().getLimits()

            # Set horizontal histo limits
            self.parent._histoHPlot.getXAxis().setLimits(xMin, xMax)

            # Set vertical histo limits
            self.parent._histoVPlot.getYAxis().setLimits(yMin, yMax)

            self._updateRadarView()

            self._updatingLimits = False

    def _updateRadarView(self):
        """Update radar view visible area.

        Takes care of y coordinate conversion.
        """
        xMin, xMax = self.getXAxis().getLimits()
        yMin, yMax = self.getYAxis().getLimits()
        self.parent._radarView.setVisibleRect(xMin, yMin, xMax - xMin, yMax - yMin)

    def _activeImageChangedSlot(self, previous, legend):
        """Handle Plot active image change.
        """
        self._dirtyCache()

        # Clear profile graph
        self.parent._histoHPlot.remove(kind='curve')
        self.parent._histoVPlot.remove(kind='curve')

        """
        widget = self.getWidgetHandle()
        position = widget.mapFromGlobal(qt.QCursor.pos())
        xPixel, yPixel = position.x(), position.y()
        dataPos = self.pixelToData(xPixel, yPixel, check=True)

        if dataPos is not None:
            x, y = dataPos
            self._updateProfiles(int(y), int(x))
        """

    def _getImageValue(self, x, y):
        """Get status bar value of top most image at position (x, y)

        :param float x: X position in plot coordinates
        :param float y: Y position in plot coordinates
        :return: The value at that point or '-'
        """
        image = self.getActiveImage()
        value = image.getData(copy=False)[int(y), int(x)]
        # print("here! value = ", value)
        return value

    def _getImageDims(self, *args):
        activeImage = self.getActiveImage()
        if (activeImage is not None and
                    activeImage.getData(copy=False) is not None):
            dims = activeImage.getData(copy=False).shape[1::-1]
            return 'x'.join(str(dim) for dim in dims)
        else:
            return '-'


class ImageViewer(qt.QMainWindow):
    """Image Viewer with x,y line profile graphy"""
    def __init__(self, parent=None, backend=None,
                 resetzoom=True, autoScale=False,
                 logScale=False, grid=False,
                 curveStyle=False, colormap=True,
                 aspectRatio=True, yInverted=True,
                 copy=True, save=True, print_=True,
                 control=False, position=False,
                 roi=False, mask=False, fit=False):

        super(ImageViewer, self).__init__(parent)

        self._plot = ImageView(self)
        self._initWidgets(backend)

        # Sync PlotBackend and ImageView
        self._plot._updateYAxisInverted()

        # Init actions
        self.group = qt.QActionGroup(self)
        self.group.setExclusive(False)

        self.resetZoomAction = self.group.addAction(
            actions.control.ResetZoomAction(self._plot, parent=self))
        self.resetZoomAction.setVisible(resetzoom)
        self.addAction(self.resetZoomAction)

        self.zoomInAction = actions.control.ZoomInAction(self._plot, parent=self)
        self.addAction(self.zoomInAction)

        self.zoomOutAction = actions.control.ZoomOutAction(self._plot, parent=self)
        self.addAction(self.zoomOutAction)

        self.xAxisAutoScaleAction = self.group.addAction(
            actions.control.XAxisAutoScaleAction(self._plot, parent=self))
        self.xAxisAutoScaleAction.setVisible(autoScale)
        self.addAction(self.xAxisAutoScaleAction)

        self.yAxisAutoScaleAction = self.group.addAction(
            actions.control.YAxisAutoScaleAction(self._plot, parent=self))
        self.yAxisAutoScaleAction.setVisible(autoScale)
        self.addAction(self.yAxisAutoScaleAction)

        self.xAxisLogarithmicAction = self.group.addAction(
            actions.control.XAxisLogarithmicAction(self._plot, parent=self))
        self.xAxisLogarithmicAction.setVisible(logScale)
        self.addAction(self.xAxisLogarithmicAction)

        self.yAxisLogarithmicAction = self.group.addAction(
            actions.control.YAxisLogarithmicAction(self._plot, parent=self))
        self.yAxisLogarithmicAction.setVisible(logScale)
        self.addAction(self.yAxisLogarithmicAction)

        self.gridAction = self.group.addAction(
            actions.control.GridAction(self._plot, gridMode='both', parent=self))
        self.gridAction.setVisible(grid)
        self.addAction(self.gridAction)

        self.curveStyleAction = self.group.addAction(
            actions.control.CurveStyleAction(self._plot, parent=self))
        self.curveStyleAction.setVisible(curveStyle)
        self.addAction(self.curveStyleAction)

        self.colormapAction = self.group.addAction(
            actions.control.ColormapAction(self._plot, parent=self))
        self.colormapAction.setVisible(colormap)
        self.addAction(self.colormapAction)

        self.colorbarAction = self.group.addAction(
            actions_control.ColorBarAction(self._plot, parent=self))
        self.colorbarAction.setVisible(False)
        self.addAction(self.colorbarAction)
        # self._colorbar.setVisible(False)

        self.keepDataAspectRatioButton = PlotToolButtons.AspectToolButton(
            parent=self, plot=self._plot)
        self.keepDataAspectRatioButton.setVisible(aspectRatio)

        self.yAxisInvertedButton = PlotToolButtons.YAxisOriginToolButton(
            parent=self, plot=self._plot)
        self.yAxisInvertedButton.setVisible(yInverted)

        self.group.addAction(self._plot.getRoiAction())
        self._plot.getRoiAction().setVisible(roi)

        self.group.addAction(self._plot.getMaskAction())
        self._plot.getMaskAction().setVisible(mask)

        # lazy loaded actions needed by the controlButton menu
        self._consoleAction = None
        self._statsAction = None
        self._panWithArrowKeysAction = None
        self._crosshairAction = None

        # Creating the toolbar also create actions for toolbuttons
        self._interactiveModeToolBar = tools.InteractiveModeToolBar(
            parent=self, plot=self._plot)
        self.addToolBar(self._interactiveModeToolBar)

        self._toolbar = self._createToolBar(title='Plot', parent=self)
        self.addToolBar(self._toolbar)

        self._outputToolBar = tools.OutputToolBar(parent=self, plot=self._plot)
        self._outputToolBar.getCopyAction().setVisible(copy)
        self._outputToolBar.getSaveAction().setVisible(save)
        self._outputToolBar.getPrintAction().setVisible(print_)
        self.addToolBar(self._outputToolBar)

        self.profile = ProfileToolBar(plot=self._plot)
        self.addToolBar(self.profile)

        # Add limits tool bar from silx.gui.plot.PlotTools
        limitsToolBar = tools.LimitsToolBar(parent=self, plot=self._plot)
        self.addToolBar(qt.Qt.BottomToolBarArea, limitsToolBar)

        # Activate shortcuts in PlotWindow widget:
        for toolbar in (self._interactiveModeToolBar, self._outputToolBar):
            for action in toolbar.actions():
                self.addAction(action)

    def getData(self):
        image = self._plot.getImage()
        if image:
            return image.getData()
        else:
            return None

    def _initWidgets(self, backend):
        """Set-up layout and plots."""
        self._histoHPlot = PlotWidget(backend=backend, parent=self)
        self._histoHPlot.getWidgetHandle().setMinimumHeight(
            self._plot.HISTOGRAMS_HEIGHT)
        self._histoHPlot.getWidgetHandle().setMaximumHeight(
            self._plot.HISTOGRAMS_HEIGHT)
        self._histoHPlot.setInteractiveMode('zoom')
        self._histoHPlot.setGraphXLabel('')
        self._histoHPlot.setGraphYLabel('')
        self._histoHPlot.sigPlotSignal.connect(self._plot._histoHPlotCB)

        self._plot.setPanWithArrowKeys(True)

        self._plot.setInteractiveMode('zoom')  # Color set in setColormap
        self._plot.sigPlotSignal.connect(self._plot._imagePlotCB)
        self._plot.getYAxis().sigInvertedChanged.connect(self._plot._updateYAxisInverted)
        self._plot.sigActiveImageChanged.connect(self._plot._activeImageChangedSlot)

        self._histoVPlot = PlotWidget(backend=backend, parent=self)
        self._histoVPlot.getWidgetHandle().setMinimumWidth(
            self._plot.HISTOGRAMS_HEIGHT)
        self._histoVPlot.getWidgetHandle().setMaximumWidth(
            self._plot.HISTOGRAMS_HEIGHT)
        self._histoVPlot.setInteractiveMode('zoom')
        self._histoVPlot.setGraphXLabel('')
        self._histoVPlot.setGraphYLabel('')
        self._histoVPlot.sigPlotSignal.connect(self._plot._histoVPlotCB)

        self._radarView = RadarView(parent=self)
        # self._radarView.visibleRectDragged.connect(self._plot._radarViewCB)

        layout = qt.QGridLayout()
        layout.addWidget(self._plot.getWidgetHandle(), 0, 0)
        layout.addWidget(self._histoVPlot.getWidgetHandle(), 0, 1)
        layout.addWidget(self._histoHPlot.getWidgetHandle(), 1, 0)
        layout.addWidget(self._radarView, 1, 1, 1, 2)
        layout.addWidget(self._plot.getColorBarWidget(), 0, 2)
        layout.addWidget(self._plot.getPositionInfoWidget(), 2, 0, 1, 2)

        layout.setColumnMinimumWidth(0, self._plot.IMAGE_MIN_SIZE)
        layout.setColumnStretch(0, 1)
        layout.setColumnMinimumWidth(1, self._plot.HISTOGRAMS_HEIGHT)
        layout.setColumnStretch(1, 0)

        layout.setRowMinimumHeight(0, self._plot.IMAGE_MIN_SIZE)
        layout.setRowStretch(0, 1)
        layout.setRowMinimumHeight(1, self._plot.HISTOGRAMS_HEIGHT)
        layout.setRowStretch(1, 0)

        layout.setSpacing(0)
        layout.setContentsMargins(0, 0, 0, 0)

        centralWidget = qt.QWidget(self)
        centralWidget.setLayout(layout)
        self.setCentralWidget(centralWidget)

    def _createToolBar(self, title, parent):
        """Create a QToolBar from the QAction of the PlotWindow.

        :param str title: The title of the QMenu
        :param qt.QWidget parent: See :class:`QToolBar`
        """
        toolbar = qt.QToolBar(title, parent)

        # Order widgets with actions
        objects = self.group.actions()

        # Add push buttons to list
        index = objects.index(self.colormapAction)
        objects.insert(index + 1, self.keepDataAspectRatioButton)
        objects.insert(index + 2, self.yAxisInvertedButton)

        for obj in objects:
            if isinstance(obj, qt.QAction):
                toolbar.addAction(obj)
            else:
                # Add action for toolbutton in order to allow changing
                # visibility (see doc QToolBar.addWidget doc)
                if obj is self.keepDataAspectRatioButton:
                    self.keepDataAspectRatioAction = toolbar.addWidget(obj)
                elif obj is self.yAxisInvertedButton:
                    self.yAxisInvertedAction = toolbar.addWidget(obj)
                else:
                    raise RuntimeError()
        return toolbar




if __name__ == "__main__":
    import sys
    from skimage import data

    camera = data.camera()

    app = qt.QApplication([])
    mainWindow = ImageViewer()
    mainWindow._plot.setImage(camera)
    mainWindow.show()
    app.exec_()
