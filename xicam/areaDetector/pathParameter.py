from os.path import isdir, isfile, expanduser

from silx.gui import qt
from silx.gui.icons import getQIcon

from pyqtgraph.widgets.SpinBox import SpinBox
from pyqtgraph.widgets.ColorButton import ColorButton
from pyqtgraph.parametertree.Parameter import Parameter
from pyqtgraph.parametertree.parameterTypes import WidgetParameterItem as _WidgetParameterItem, \
                                                   SimpleParameter as _SimpleParamter

class WidgetParameterItem(_WidgetParameterItem):
    def __init__(self, *args, **kwargs):
        super(WidgetParameterItem, self).__init__(*args, **kwargs)

    def optsChanged(self, param, opts):
        """Called when any options are changed that are not
        name, value, default, or limits"""
        #print "opts changed:", opts
        super(_WidgetParameterItem, self).optsChanged(param, opts)

        if 'readonly' in opts:
            self.updateDefaultBtn()
            if isinstance(self.widget, (qt.QCheckBox,ColorButton)):
                self.widget.setEnabled(not opts['readonly'])

        ## If widget is a SpinBox, pass options straight through
        if isinstance(self.widget, SpinBox):
            # send only options supported by spinbox
            sbOpts = {}
            if 'units' in opts and 'suffix' not in opts:
                sbOpts['suffix'] = opts['units']
            for k,v in opts.items():
                if k in self.widget.opts:
                    # visible option is not available on spinbox
                    if k != 'visible':
                        sbOpts[k] = v
            self.widget.setOpts(**sbOpts)
            self.updateDisplayLabel()

class SimpleParameter(_SimpleParamter):
    itemClass = WidgetParameterItem

    def __init__(self, *args, **kwargs):
        super(SimpleParameter, self).__init__(*args, **kwargs)


class LabelParameterItem(WidgetParameterItem):
    def __init__(self, param, depth):
        WidgetParameterItem.__init__(self, param, depth)
        self.hideWidget = True
        self.setExpanded(True)


class LabelParameter(Parameter):
    """parent parameter."""
    itemClass = LabelParameterItem


class FilePathParameterItem(WidgetParameterItem):
    """
    Customized WidgetParameterItem with pathBtn
    """

    def __init__(self, *args, **kwargs):
        super(FilePathParameterItem, self).__init__(*args, **kwargs)

        self.pathBtn = qt.QPushButton()
        self.pathBtn.setFixedWidth(20)
        self.pathBtn.setFixedHeight(20)
        self.pathBtn.setIcon(getQIcon('document-open'))
        self.pathBtn.clicked.connect(self.btnClicked)

        # Put self.pathBtn in front of default Btn
        wgCnt = self.layoutWidget.layout().count() - 1
        self.layoutWidget.layout().insertWidget(wgCnt, self.pathBtn)

    def btnClicked(self):
        self.param.activate()


class FilePathParameter(SimpleParameter):
    itemClass = FilePathParameterItem
    currentPath = expanduser('~')

    def __init__(self, *args, **kwargs):
        super(FilePathParameter, self).__init__(*args, **kwargs)
        if 'fileType' in kwargs.keys():
            self.fileType = kwargs['fileType']
        else:
            self.fileType = "hdr (*.hdr)"

    def activate(self):
        _dir = self.currentPath if isdir(self.currentPath) else expanduser('~')

        selectedPath = qt.QFileDialog.getOpenFileName(None,
                                                      "Please select a file",
                                                      _dir,
                                                      self.fileType)
        # Do not update path string if selection exists
        path = selectedPath[0]
        if isfile(path):
            self.setValue(path)


class DirPathParameterItem(WidgetParameterItem):
    """
    Customized WidgetParameterItem with pathBtn
    """

    sigSelect = qt.Signal()

    def __init__(self, *args, **kwargs):
        super(DirPathParameterItem, self).__init__(*args, **kwargs)

        self.pathBtn = qt.QPushButton()
        self.pathBtn.setFixedWidth(20)
        self.pathBtn.setFixedHeight(20)
        self.pathBtn.setIcon(getQIcon('document-open'))
        self.pathBtn.clicked.connect(self.btnClicked)

        # Put self.pathBtn in front of default Btn
        wgCnt = self.layoutWidget.layout().count() - 1
        self.layoutWidget.layout().insertWidget(wgCnt, self.pathBtn)

    def btnClicked(self):
        self.param.activate()


class DirPathParameter(SimpleParameter):
    itemClass = DirPathParameterItem
    currentPath = expanduser('~')

    def __init__(self, *args, **kwargs):
        super(DirPathParameter, self).__init__(*args, **kwargs)

    def activate(self):
        _dir = self.currentPath if isdir(self.currentPath) else expanduser('~')

        selectedPath = qt.QFileDialog.getExistingDirectory(None,
                                                      "Please select a directory",
                                                      _dir,
                                                      qt.QFileDialog.ShowDirsOnly)
        # Do not update path string if selection exists
        path = selectedPath
        if isdir(path):
            self.setValue(path)


class TextParameterItem(WidgetParameterItem):
    def __init__(self, param, depth):
        WidgetParameterItem.__init__(self, param, depth)
        self.hideWidget = False
        self.subItem = qt.QTreeWidgetItem()
        self.addChild(self.subItem)

    def treeWidgetChanged(self):
        ## TODO: fix so that superclass method can be called
        ## (WidgetParameter should just natively support this style)
        #WidgetParameterItem.treeWidgetChanged(self)
        self.treeWidget().setFirstItemColumnSpanned(self.subItem, True)
        self.treeWidget().setItemWidget(self.subItem, 0, self.textBox)

        # for now, these are copied from ParameterItem.treeWidgetChanged
        self.setHidden(not self.param.opts.get('visible', True))
        self.setExpanded(self.param.opts.get('expanded', True))

    def makeWidget(self):
        self.textBox = qt.QTextEdit()
        self.textBox.setMaximumHeight(300)
        self.textBox.setReadOnly(self.param.opts.get('readonly', False))
        self.textBox.value = lambda: str(self.textBox.toPlainText())
        self.textBox.setValue = self.textBox.setPlainText
        self.textBox.sigChanged = self.textBox.textChanged
        return self.textBox

class TextParameter(Parameter):
    """Editable string; displayed as large text box in the tree."""
    itemClass = TextParameterItem
