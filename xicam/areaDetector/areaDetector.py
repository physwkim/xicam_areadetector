import os
import atexit
import logging
import time
import datetime

import numpy as np
import epics

import fabio
import pyqtgraph as pg
from pyqtgraph.parametertree import Parameter, ParameterTree

from silx.gui import qt
from silx.gui.plot.PlotWidget import PlotWidget
from pyFAI.gui.CalibrationContext import CalibrationContext
from pyFAI.ext.invert_geometry import InvertGeometry
from pyFAI.gui.model.GeometryModel import GeometryModel
from pyFAI.io import DefaultAiWriter
from pyFAI.azimuthalIntegrator import AzimuthalIntegrator
from pyFAI import detector_factory
from pyFAI.gui.model.DetectorModel import DetectorModel

from xicam.core import msg, threads
from xicam.core.msg import ERROR
from xicam.plugins import GUIPlugin, GUILayout
from xicam.plugins.guiplugin import PanelState
from xicam.areaDetector.pathParameter import FilePathParameter, DirPathParameter
from xicam.areaDetector.Plot1D import Plot1D
from xicam.areaDetector.Plot2D import Plot2D
from xicam.areaDetector.ImageViewCustom import ImageViewer
from xicam.areaDetector.integrationPlotCustom import IntegrationPlot
from xicam.areaDetector.settingsPanel import settingsPanel
from xicam.areaDetector.dataResourceBrowser import DataBrowser, LocalFileSystemTree, DataFileTree
from xicam.areaDetector.workflows.integrateWorkFlow import IntegrateWorkflow

logger = logging.getLogger(__name__)

ACQUIRE = 'cam1:Acquire'
ARRAY = 'image1:ArrayData'
XSIZE = 'cam1:MaxSizeX_RBV'
YSIZE = 'cam1:MaxSizeY_RBV'

logger.setLevel(logging.INFO)


class areaDetectorPlugin(GUIPlugin):
    """A xicam plugin for areadetector viewer and azimuthal integration"""
    name = 'areaDetector'

    def __init__(self):

        # Internal variables
        self.__lastImageFile = None
        self.__lastRawImage = None
        self.__lastProcessedImage = None
        self.__lastBackgroundFile = None
        self.__reslut1d = None
        self.__result2d = None
        self.__azimuthalIntegrator = None
        self.__curves = None

        # Background data
        self.bgData = None

        self.lastUpdateTime = time.time()
        self.lock = True

        # File Browser
        self.fileBrowser = DataBrowser(LocalFileSystemTree())
        self.fileBrowser.sigOpen.connect(self.loadImage)
        self.fileBrowser.sigPathChanged.connect(self.changeCurrentPath)

        # Data Browser
        self.dataBrowser = DataBrowser(DataFileTree())
        self.dataBrowser.sigOpen.connect(self.plotCurve)
        self.dataBrowser.sigAppend.connect(self.addCurve)

        # Settings
        self.settings = settingsPanel(self)

        # Image Viewer
        self.plotWidget = ImageViewer()

        # QSettings
        qsettings = qt.QSettings(qt.QSettings.IniFormat,
                                 qt.QSettings.UserScope,
                                 "pyfai",
                                 "pyfai-calib2",
                                 None)

        # Viewer in Compare panel
        self.plot1D = Plot1D()
        self.plot1D.getLegendsDockWidget().show()

        # Integration Viewer in ImageView
        self.integratePlot = PlotWidget()
        self.integratePlot.setGraphXLabel('Radial')
        self.integratePlot.setGraphYLabel('Intensity')

        # Dummy Widget holder
        self.dualGraphWidget = qt.QWidget()
        self.dualGraphWidget.setLayout(qt.QHBoxLayout())
        self.calibContext = CalibrationContext(qsettings)
        self.calibContext.setParent(self.dualGraphWidget)
        self.integPlot = IntegrationPlot(self.dualGraphWidget)
        self.dualGraphWidget.layout().addWidget(self.integPlot)

        self.stages = {
            'Image View': GUILayout(self.plotWidget,
                                    lefttop=self.fileBrowser,
                                    bottom=self.integratePlot,
                                    left=self.settings),
            'Integration': GUILayout(self.dualGraphWidget,
                                     lefttop=self.fileBrowser,
                                     left=self.settings),
            'Compare': GUILayout(self.plot1D,
                                 lefttop=self.dataBrowser,
                                 left=PanelState.Disabled)
        }

        super(areaDetectorPlugin, self).__init__()

        self.settings.bgPath.sigValueChanged.connect(self.updateBg)
        self.settings.prefixPV.sigValueChanged.connect(self.changePV)

        prefix = self.settings.prefixPV.value().strip()
        self.acquirePV = epics.PV(
            prefix + ACQUIRE, callback=self.releaseLock)
        self.arrayPV = epics.PV(prefix + ARRAY)
        self.arraySizeX = epics.PV(prefix + XSIZE)
        self.arraySizeY = epics.PV(prefix + YSIZE)

        # Initialize workflows
        self.integrateWorkflow = IntegrateWorkflow()

        # Workflow connections
        self.settings.sigIntegrate.connect(self.doIntegrate)

        # Update Initial Path to PathParameters
        path = self.fileBrowser.URILineEdit.text()
        self.changeCurrentPath(path)

        # Update thread from areaDetector IOC
        self.imageThread = threads.QThreadFuture(self.updateImage,
                                                 threadkey='updateImage',
                                                 showBusy=False)
        self.imageThread.start()

        # Delete Subprocess At Exit
        atexit.register(self.cleanup)

    @threads.method()
    def doIntegrate(self):
        args = {}

        # Get raw image
        image = self.__lastRawImage

        # Subtract background
        if self.settings.bgCB.value() and image is not None:
            if os.path.isfile(self.settings.bgPath.value()):
                try:
                    bg = fabio.open(self.settings.bgPath.value()).data
                except:
                    bg = None
            if bg is not None and bg.shape == image.shape:
                image = np.subtract(image, bg)

        args['data'] = image

        # If image is None, display warning
        if args['data'] is None:
            threads.invoke_in_main_thread(qt.QMessageBox.warning,
                                          self.plotWidget,          # parent
                                          "Warning",                # title
                                          "Image is not selected!")  # text
            return

        maskFile = self.settings.mask.value()
        if maskFile is not "":
            if os.path.isfile(maskFile):
                try:
                    mask = fabio.open(maskFile).data
                except:
                    mask = None
            else:
                mask = None
        else:
            mask = None

        args['mask'] = mask
        args['distance'] = self.settings.distance.value()
        args['poni1'] = self.settings.poni1.value()
        args['poni2'] = self.settings.poni2.value()
        args['rot1'] = self.settings.rot1.value()
        args['rot2'] = self.settings.rot2.value()
        args['rot3'] = self.settings.rot3.value()
        args['detector'] = detector_factory(self.settings.detector.value())
        args['detector'].guess_binning(image)
        args['wavelength'] = self.settings.wavelength.value()
        args['npt_rad'] = self.settings.radialPoints.value()
        args['npt_azim'] = self.settings.azimuthalPoints.value()
        args['unit'] = self.settings.radialUnit.value()
        args['method'] = self.settings.method.value()

        splineFile = self.settings.spline.value()
        if os.path.isfile(splineFile):
            args['spline'] = self.settings.spline.value()
        else:
            args['spline'] = None

        if self.settings.polCB.value():
            args['polarization_factor'] = self.settings.polz_factor.value()
        else:
            args['polarization_factor'] = None

        param = [args['distance'], args['poni1'], args['poni2'],
                 args['rot1'], args['rot2'], args['rot3'], args['wavelength']]

        if self.__lastProcessedImage is not None:
            _need_new = not (self.__lastProcessedImage.shape ==
                             args['data'].shape)
        else:
            _need_new = True

        # Check azimuthalIntegrator
        if self.__azimuthalIntegrator is None or _need_new:
            logger.warning("Make a new azimuthal integrator")
            self.__azimuthalIntegrator = AzimuthalIntegrator(dist=args['distance'],
                                                             poni1=args['poni1'],
                                                             poni2=args['poni2'],
                                                             rot1=args['rot1'],
                                                             rot2=args['rot2'],
                                                             rot3=args['rot3'],
                                                             detector=args['detector'],
                                                             wavelength=args['wavelength'],
                                                             splineFile=args['spline'])

            # Update fittedGeometry
            geometry = GeometryModel()
            geometry.distance().setValue(self.settings.distance.value())
            geometry.wavelength().setValue(self.settings.wavelength.value())
            geometry.poni1().setValue(self.settings.poni1.value())
            geometry.poni2().setValue(self.settings.poni2.value())
            geometry.rotation1().setValue(self.settings.rot1.value())
            geometry.rotation2().setValue(self.settings.rot2.value())
            geometry.rotation3().setValue(self.settings.rot3.value())
            self.calibContext.getCalibrationModel().fittedGeometry().setFrom(geometry)

            # Invert geometry
            invertGeometry = InvertGeometry(self.__azimuthalIntegrator.array_from_unit(typ="center",
                                                                                       unit=self.settings.radialUnit.value(),
                                                                                       scale=True),
                                            np.rad2deg(self.__azimuthalIntegrator.chiArray()))
            # Set InvertGeometry
            self.integPlot.setInverseGeometry(invertGeometry)

            # Set Geometry
            self.integPlot.setGeometry(self.__azimuthalIntegrator)
            self.integPlot.setDirectDist(args['distance'])
            self.integPlot.setRadialUnit(args['unit'])
            self.integPlot.setWavelength(args['wavelength'])

        else:
            if not np.all(np.isclose(self.__azimuthalIntegrator.param, param[:6], atol=0.0, rtol=1e-10)) or \
                    not np.isclose(self.__azimuthalIntegrator.get_wavelength(), args['wavelength'], atol=0.0, rtol=1e-10) or \
                    self.__azimuthalIntegrator.get_splineFile() != args['spline']:

                logger.debug(
                    "AzimuthalIntegrator is already exists. Updating...")
                self.__azimuthalIntegrator.set_param(param)

                # Update fittedGeometry
                geometry = GeometryModel()
                geometry.distance().setValue(self.settings.distance.value())
                geometry.wavelength().setValue(self.settings.wavelength.value())
                geometry.poni1().setValue(self.settings.poni1.value())
                geometry.poni2().setValue(self.settings.poni2.value())
                geometry.rotation1().setValue(self.settings.rot1.value())
                geometry.rotation2().setValue(self.settings.rot2.value())
                geometry.rotation3().setValue(self.settings.rot3.value())
                self.calibContext.getCalibrationModel().fittedGeometry().setFrom(geometry)

                # Invert geometry
                try:
                    invertGeometry = InvertGeometry(self.__azimuthalIntegrator.array_from_unit(typ="center",
                                                                                               unit=self.settings.radialUnit.value(), scale=True),
                                                    np.rad2deg(self.__azimuthalIntegrator.chiArray()))
                except:
                    logger.debug("Make a new azimuthal integrator")
                    self.__azimuthalIntegrator = AzimuthalIntegrator(dist=args['distance'],
                                                                     poni1=args['poni1'],
                                                                     poni2=args['poni2'],
                                                                     rot1=args['rot1'],
                                                                     rot2=args['rot2'],
                                                                     rot3=args['rot3'],
                                                                     detector=args['detector'],
                                                                     wavelength=args['wavelength'],
                                                                     splineFile=args['spline'])

                    invertGeometry = InvertGeometry(self.__azimuthalIntegrator.array_from_unit(typ="center",
                                                                                               unit=self.settings.radialUnit.value(), scale=True),
                                                    np.rad2deg(self.__azimuthalIntegrator.chiArray()))

                # Set InvertGeometry
                logger.debug("Updating invertGeometry")
                self.integPlot.setInverseGeometry(invertGeometry)

                logger.debug("setParameters in integPlot")
                # Set Geometry
                self.integPlot.setGeometry(self.__azimuthalIntegrator)
                self.integPlot.setDirectDist(args['distance'])
                self.integPlot.setRadialUnit(args['unit'])
                self.integPlot.setWavelength(args['wavelength'])

        self.integrateWorkflow.execute(None,
                                       args=args,
                                       ai=self.__azimuthalIntegrator,
                                       threadkey='integrateWorkflow',
                                       callback_slot=self.updateIntegrationData)

        self.__lastProcessedImage = args['data']

    def releaseLock(self, **kwargs):
        """image update thread lock"""
        if self.acquirePV.get() == 0:
            self.lock = False

    def updateIntegrationData(self, *args):
        """Update plots and save data"""
        outputs = args[0]

        # radial, intensity
        self.__result1d = outputs['result1d'].value

        # intensity, radial, azimuthal
        self.__result2d = outputs['result2d'].value

        # Draw 1D graph in Integration Panel
        threads.invoke_in_main_thread(
            self.integPlot.setHistogram, self.__result1d)

        # Draw 2D graph in Integration Panel
        threads.invoke_in_main_thread(self.integPlot.setImage, self.__result2d)

        # Save 1D integrated data
        radial = self.__result1d[0]
        intensity = self.__result1d[1]

        if self.__lastImageFile is not None:
            imageFile = self.__lastImageFile
        else:
            prefix = self.settings.prefixPV.value().strip()

            try:
                file_name = epics.caget(prefix + "TIFF1:FullFileName_RBV", as_string=True)
                basename = os.path.basename(file_name)
                imageFile = os.path.join(self.fileBrowser.URILineEdit.text(), basename)
            except:
                imageFile = os.path.join(self.fileBrowser.URILineEdit.text(), 'image.dat')

        data1DFileName = os.path.splitext(imageFile)[0]

        dirname = os.path.dirname(imageFile)
        temp = []
        for file_ in os.listdir(dirname):
            if file_.startswith(os.path.basename(data1DFileName)):
                temp.append(file_)
        num = len(temp) + 1
        data1DFile = data1DFileName + '{:03d}'.format(num) + '.dat'

        # Draw 1D result in ImageView Panel
        threads.invoke_in_main_thread(
            self.integratePlot.addCurve, radial, intensity, legend='data')
        threads.invoke_in_main_thread(
            self.integratePlot.setGraphTitle, data1DFile)

        # Always remove existing file
        if os.path.isfile(data1DFile):
            try:
                os.remove(data1DFile)
            except:
                qt.QMessageBox.warning(self.plotWidget,  # parent
                                       "Warning",        # title
                                       "Could not delete a existing file!")  # text
                return

        writer = DefaultAiWriter(data1DFile, self.__azimuthalIntegrator)
        writer.save1D(data1DFile, radial, intensity)

    def changePV(self):
        """Change EPICS prefix of the areaDetector"""
        # clear callback
        logger.debug("Clear and disconnect previous PVs")
        self.acquirePV.disconnect()
        self.arrayPV.disconnect()
        self.arraySizeX.disconnect()
        self.arraySizeY.disconnect()
        logger.debug("Done clearing")

        logger.info("changing PV")
        prefix = self.settings.prefixPV.value().strip()
        self.acquirePV = epics.PV(
            prefix + ACQUIRE, callback=self.releaseLock)
        self.arrayPV = epics.PV(prefix + ARRAY)
        self.arraySizeX = epics.PV(prefix + XSIZE)
        self.arraySizeY = epics.PV(prefix + YSIZE)
        logger.info("done changing")

    def changeCurrentPath(self, path):
        """ Change sub-widget's path to dataBrowser's current path """
        FilePathParameter.currentPath = path
        DirPathParameter.currentPath = path

        # Set current fileBrowser's path to dataBrowser
        self.dataBrowser.setPath(path)

    def cleanup(self):
        """ Delete Subprocess At Exit """
        if self.settings.calib2Thread and self.settings.calib2Thread.running:
            logger.debug("Deleting sub-threads")
            self.settings.calib2Proc.kill()

        logger.debug("disconnecting PVs")
        try:
            self.acquirePV.disconnect()
            self.arrayPV.disconnect()
            self.arraySizeX.disconnect()
            self.arraySizeY.disconnect()
        except:
            logger.critical("Error occured on cleanup")

    def updateBg(self):
        """ Update background image """
        logger.debug("updating background")
        fileName = self.settings.bgPath.value()
        try:
            if os.path.isfile(fileName):
                self.bgData = fabio.open(fileName).data
                self.__lastBackgroundFile = fileName
                self.settings.bgPath.setValue(fileName)

            else:
                self.bgData = None
                self.settings.bgPath.setValue('')
                logger.warning("Background file is not exists")
        except:
            logger.warning("Error during background loading")

        # Reload image with background subtracted
        data = np.subtract(self.__lastRawImage, self.bgData)
        threads.invoke_in_main_thread(self.plotWidget._plot.setImage, data)

    def parse(self, filename):
        """ Parse the contents of a file """
        try:
            with open(filename, 'r') as File:
                contents = File.readlines()
                idx = 0

                # search label index
                for line in contents:
                    if line.startswith('#'):
                        idx += 1
                    else:
                        break

                # parse label
                if idx == 0:
                    if contents[idx].startswith('#'):
                        label = contents[idx][1:].strip().split()
                        data = np.loadtxt(filename)
                    else:
                        label = contents[idx].strip().split(';')
                        data = np.loadtxt(filename, skiprows=1, delimiter=';')
                else:
                    label = contents[idx-1][1:].strip().split()
                    data = np.loadtxt(filename)

            return label, data

        except:
            return None, None

    def plotCurve(self, filenames):
        """Clear curves and add a curve from a file"""
        # Clear existing curves
        threads.invoke_in_main_thread(self.plot1D.clearCurves)
        self.addCurve(filenames)

    def addCurve(self, filenames):
        """parse a file and add a curve"""
        curves = self.plot1D.getAllCurves()
        if isinstance(filenames, (list)):
            idx = 0
            for fileN in filenames:
                label, data = self.parse(fileN)

                if label is None:
                    logger.warning("This file is not valid : {}".format(fileN))
                    continue

                threads.invoke_in_main_thread(self.plot1D.addCurve,
                                              data[:, 0],
                                              data[:, 1],
                                              legend=os.path.basename(fileN))

                if idx == 0 and not len(curves):
                    threads.invoke_in_main_thread(self.plot1D.setGraphXLabel,
                                                  label[0])
                    threads.invoke_in_main_thread(self.plot1D.setGraphYLabel,
                                                  label[1])

                idx += 1
        else:
            label, data = self.parse(filenames)

            if label is None:
                logger.warning("This file is not valid : {}".format(filenames))
                return

            if not len(curves):
                threads.invoke_in_main_thread(self.plot1D.setGraphXLabel,
                                              label[0])
                threads.invoke_in_main_thread(self.plot1D.setGraphYLabel,
                                              label[1])

            threads.invoke_in_main_thread(self.plot1D.addCurve,
                                          data[:, 0],
                                          data[:, 1],
                                          legend=os.path.basename(filenames))

    def updateImage(self):
        """A threads that update images from a epics PV"""
        while(1):
            if not self.lock:
                try:
                    if self.arrayPV.connected:
                        time.sleep(0.1)
                        data = self.arrayPV.get(timeout=5, use_monitor=False)
                        self.__lastImageFile = None
                    else:
                        data = None
                        continue

                    if self.arraySizeX.connected:
                        xsize = self.arraySizeX.get()
                    else:
                        xsize = None
                        continue

                    if self.arraySizeY.connected:
                        ysize = self.arraySizeY.get()
                    else:
                        ysize = None
                        continue
                except Exception as e:
                    print("In UpdateImage {}".format(e))

                if data is not None:
                    if len(data) == xsize*ysize:
                        data = data.reshape(ysize, xsize)
                        self.__lastRawImage = data
                orig_data = self.plotWidget._plot.getImage()

                if not np.all(data == orig_data):
                    now = time.time()
                    diff = now - self.lastUpdateTime

                    # Is updateCB checked?
                    update = self.settings.autoUpdateCB.value()

                    # Is backgroundCB checked?
                    background = self.settings.bgCB.value()

                    if background:
                        try:
                            shape = self.bgData.shape

                            if (shape[0] == data.shape[0]) and \
                                    (shape[1] == data.shape[1]):
                                data = np.subtract(data, self.bgData)

                        except:
                            logger.warning("error during subtract")

                    if diff > 0.01 and update:
                        threads.invoke_in_main_thread(
                            self.plotWidget._plot.setImage, data, reset=False)
                        self.lastUpdateTime = now

                        # Graph title
                        now = datetime.datetime.today().strftime(
                            '[%Y-%m-%d %H:%M:%S]')
                        title = now + self.arrayPV.pvname
                        threads.invoke_in_main_thread(
                            self.plotWidget._plot.setGraphTitle, title)

                        # Auto update
                        autoUpdate = self.settings.autoIntegrateCB.value()
                        existPoniFile = os.path.isfile(
                            self.settings.calibPoniFile.value())

                        if autoUpdate and existPoniFile:
                            self.doIntegrate()

                self.lock = True
            else:
                time.sleep(0.001)

    def loadImage(self, filename):
        """Load from a file and display on a image viewer"""
        if isinstance(filename, (list)):
            fileN = filename[0]
        else:
            fileN = filename

        if os.path.isfile(fileN):
            try:
                data = fabio.open(fileN).data
                self.__lastImageFile = fileN
                self.__lastRawImage = data
            except:
                logger.warning("Not supported file!")
                return

        # Subtract background
        background = self.settings.bgCB.value() and self.bgData is not None

        if background:
            shape = self.bgData.shape

            if (shape[0] == data.shape[0]) and \
                    (shape[1] == data.shape[1]):
                data = np.array(data) - np.array(self.bgData)

                # Plot background subtracted image
                threads.invoke_in_main_thread(
                    self.plotWidget._plot.setImage, data)

                # Graph title
                now = datetime.datetime.today().strftime('[%Y-%m-%d %H:%M:%S]')
                title = now + os.path.basename(fileN)
                threads.invoke_in_main_thread(
                    self.plotWidget._plot.setGraphTitle, title)

                # Auto update
                autoUpdate = self.settings.autoIntegrateCB.value()
                existPoniFile = os.path.isfile(
                    self.settings.calibPoniFile.value())

                if autoUpdate and existPoniFile:
                    self.doIntegrate()

            else:
                qt.QMessageBox.warning(self.plotWidget,  # parent
                                       "Warning",        # title
                                       "Image size is differnt from background")  # text

                # Remove image if there is error
                threads.invoke_in_main_thread(self.plotWidget._plot.clear)
                threads.invoke_in_main_thread(
                    self.plotWidget._plot.setGraphTitle, '')
        else:
            # Plot Image without background
            threads.invoke_in_main_thread(self.plotWidget._plot.setImage, data)

            # Graph title
            now = datetime.datetime.today().strftime('[%Y-%m-%d %H:%M:%S]')
            title = now + os.path.basename(fileN)
            threads.invoke_in_main_thread(
                self.plotWidget._plot.setGraphTitle, title)

            # Auto update
            autoUpdate = self.settings.autoIntegrateCB.value()
            existPoniFile = os.path.isfile(self.settings.calibPoniFile.value())

            if autoUpdate and existPoniFile:
                self.doIntegrate()
