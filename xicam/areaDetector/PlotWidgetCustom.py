from silx.gui import qt
from silx.gui.plot import PlotWidget as _PlotWidget, PlotEvents, PlotInteraction
from silx.gui.plot.actions.control import ZoomBackAction, CrosshairAction,\
                                          ColormapAction

from xicam.ptychography.PlotInteractionCustom import PlotInteractionCustom

from matplotlib import rcParams
rcParams.update({'font.size': 8})


DEBUG=0
MIDDLE_PAN = False


class PlotWidget(_PlotWidget):
    # spectrum signal from ch1 to ch16
    sigSpectrumSelected = qt.Signal(object)

    def __init__(self, parent=None, backend=None):
        super(PlotWidget, self).__init__(parent, backend)
        # Retrieve PlotWidget's plot area widget
        plotArea = self.getWidgetHandle()

        # Create QAction for the context menu once for all
        self._zoomBackAction = ZoomBackAction(plot=self, parent=self)
        self._colormapAction = ColormapAction(plot=self, parent=self)

        # Set plot area custom context menu
        plotArea.setContextMenuPolicy(qt.Qt.CustomContextMenu)
        plotArea.customContextMenuRequested.connect(self._contextMenu)

        if MIDDLE_PAN:
            # Set plot area custom context menu
            plotArea.setContextMenuPolicy(qt.Qt.NoContextMenu)

            self._eventHandlerPan = PlotInteractionCustom(self)
            self._eventHandlerPan.setInteractiveMode('pan')

        self.setGraphXLabel('x (μm)')
        self.setGraphYLabel('y (μm)')

        # self._backend.ax.set_position([0.2, 0.15, 0.75, 0.75])
        # self._backend.ax2.set_position([0.2, 0.15, 0.75, 0.75])

        # invert Y-axis
        # self.setYAxisInverted(True)

        # get center
        center = self.getCenter()

        # Set default colormap to 'viridis'
        self.getDefaultColormap().setName('viridis')

    def getCenter(self):
        xLow, xHigh = self.getGraphXLimits()
        yLow, yHigh = self.getGraphYLimits()
        center = ((xHigh+xLow)/2, (yHigh+yLow)/2)
        return center

    def setParent(self, parent):
        self._parent = parent

    def setGraphTitle(self, title="", loc="center"):
        self._graphTitle = str(title)
        self._backend.ax.set_title(title, loc=loc)
        self._setDirtyPlot()

    def _contextMenu(self, pos):
        """Handle plot area customContextMenuRequested signal.

        :param QPoint pos: Mouse position relative to plot area
        """
        # Create the context menu
        menu = qt.QMenu(self)
        menu.addAction(self._zoomBackAction)
        menu.addSeparator()
        menu.addAction(self._colormapAction)
        menu.addSeparator()

        # Displaying the context menu at the mouse position requires
        # a global position.
        # The position received as argument is relative to PlotWidget's
        # plot area, and thus needs to be converted.
        plotArea = self.getWidgetHandle()
        globalPosition = plotArea.mapToGlobal(pos)
        menu.exec_(globalPosition)

    def notify(self, event, **kwargs):
        """Send an event to the listeners and send signals.

        Event are passed to the registered callback as a dict with an 'event'
        key for backward compatibility with PyMca.

        :param str event: The type of event
        :param kwargs: The information of the event.
        """
        eventDict = kwargs.copy()
        eventDict['event'] = event
        self.sigPlotSignal.emit(eventDict)

        if event == 'setKeepDataAspectRatio':
            self.sigSetKeepDataAspectRatio.emit(kwargs['state'])
        elif event == 'setGraphGrid':
            self.sigSetGraphGrid.emit(kwargs['which'])
        elif event == 'setGraphCursor':
            self.sigSetGraphCursor.emit(kwargs['state'])
        elif event == 'contentChanged':
            self.sigContentChanged.emit(
                kwargs['action'], kwargs['kind'], kwargs['legend'])
        elif event == 'activeCurveChanged':
            self.sigActiveCurveChanged.emit(
                kwargs['previous'], kwargs['legend'])
        elif event == 'activeImageChanged':
            self.sigActiveImageChanged.emit(
                kwargs['previous'], kwargs['legend'])
        elif event == 'activeScatterChanged':
            self.sigActiveScatterChanged.emit(
                kwargs['previous'], kwargs['legend'])
        elif event == 'interactiveModeChanged':
            self.sigInteractiveModeChanged.emit(kwargs['source'])

        eventDict = kwargs.copy()
        eventDict['event'] = event
        self._callback(eventDict)


    def graphCallback(self, ddict=None):
        """This callback is going to receive all the events from the plot.

        Those events will consist on a dictionary and among the dictionary
        keys the key 'event' is mandatory to describe the type of event.
        This default implementation only handles setting the active curve.
        """

        if ddict is None:
            ddict = {}
        if ddict['event'] in ["legendClicked", "curveClicked"]:
            if ddict['button'] == "left":
                self.setActiveCurve(ddict['label'])
                qt.QToolTip.showText(self.cursor().pos(), ddict['label'])

        if ddict['event'] in ['mouseMoved','MouseAt', 'mouseClicked']:
            # self._handleMouseMovedEvent(ddict)
            # print("ddict=", ddict)
            xPixel = ddict['xpixel']
            yPixel = ddict['ypixel']
            try:
                if self._isPositionInPlotArea(xPixel, yPixel) == (xPixel, yPixel):
                    data = self.getImage().getData()

                    dataPos = [ddict['x'], ddict['y']]
                    origin = self.getImage().getOrigin()
                    scale = self.getImage().getScale()

                    column = int((dataPos[0] - origin[0]) / float(scale[0]))
                    row = int((dataPos[1] - origin[1]) / float(scale[1]))

                    if scale[0] is not None:
                        x = origin[0] + column * scale[0]
                        y = origin[1] + row * scale[1]
                    else:
                        x = column
                        y = row

                if column < data.shape[1] and row < data.shape[0]:
                    self.setMouseText("%g, %g, %g" % (x, y, data[row, column]))

                # Update energy-spectrum with mouse click
                # if ddict['event'] is 'mouseClicked':
                #     pos = {'x':column, 'y':row, 'xData':x, 'yData':y}
                #     self.sigSpectrumSelected.emit(pos)
            except:
                # print("Error on displaying data tooltip on PlotWidgetCustom")
                pass

        self.sigPlotSignal.emit(ddict)


    def setMouseText(self, text=""):
        try:
            if len(text):
                qt.QToolTip.showText(self.cursor().pos(),
                                     text, self, qt.QRect())
            else:
                qt.QToolTip.hideText()
        except:
            print("Error trying to show mouse text <%s>" % text)



    def _handleMouseMovedEvent(self, ddict):
        pass
        # print("mouse moved! x=%.7g, y=%.7g"%(ddict['x'], ddict['y']))


    def onMousePress(self, xPixel, yPixel, btn):
        """Handle mouse press event.

        :param float xPixel: X mouse position in pixels
        :param float yPixel: Y mouse position in pixels
        :param str btn: Mouse button in 'left', 'middle', 'right'
        """
        if self._isPositionInPlotArea(xPixel, yPixel) == (xPixel, yPixel):
            self._pressedButtons.append(btn)
            # for mouse-zoom action
            self._eventHandler.handleEvent('press', xPixel, yPixel, btn)

            # for mouse-pan action
            if MIDDLE_PAN and btn == 'middle':
                if btn == 'right':
                    # set to the previous zoom state on mouse-right click
                    self._limitsHistory.pop()
                self._eventHandlerPan.handleEvent('press', xPixel, yPixel, btn)

    def onMouseRelease(self, xPixel, yPixel, btn):
        """Handle mouse release event.

        :param float xPixel: X mouse position in pixels
        :param float yPixel: Y mouse position in pixels
        :param str btn: Mouse button in 'left', 'middle', 'right'
        """
        try:
            self._pressedButtons.remove(btn)
        except ValueError:
            pass
        else:
            xPixel, yPixel = self._isPositionInPlotArea(xPixel, yPixel)

            # for mouse-zoom action
            self._eventHandler.handleEvent('release', xPixel, yPixel, btn)

            # for mouse-pan action
            if MIDDLE_PAN:
                self._eventHandlerPan.handleEvent('release', xPixel,
                                                  yPixel, btn)

    def onMouseMove(self, xPixel, yPixel):
        """Handle mouse move event.

        :param float xPixel: X mouse position in pixels
        :param float yPixel: Y mouse position in pixels
        """
        inXPixel, inYPixel = self._isPositionInPlotArea(xPixel, yPixel)
        isCursorInPlot = inXPixel == xPixel and inYPixel == yPixel

        if self._cursorInPlot != isCursorInPlot:
            self._cursorInPlot = isCursorInPlot
            # for mouse-zoom action
            self._eventHandler.handleEvent(
                'enter' if self._cursorInPlot else 'leave')

            # for mouse-pan action
            if MIDDLE_PAN:
                self._eventHandlerPan.handleEvent(
                    'enter' if self._cursorInPlot else 'leave')

        if isCursorInPlot:
            # Signal mouse move event
            dataPos = self.pixelToData(inXPixel, inYPixel)
            assert dataPos is not None

            btn = self._pressedButtons[-1] if self._pressedButtons else None
            event = PlotEvents.prepareMouseSignal(
                'mouseMoved', btn, dataPos[0], dataPos[1], xPixel, yPixel)
            self.notify(**event)

        # Either button was pressed in the plot or cursor is in the plot
        if isCursorInPlot or self._pressedButtons:
            # for mouse-zoom action
            self._eventHandler.handleEvent('move', inXPixel, inYPixel)

            if MIDDLE_PAN:
                # for mouse-pan action
                self._eventHandlerPan.handleEvent('move', inXPixel, inYPixel)

    def onMouseWheel(self, xPixel, yPixel, angleInDegrees):
        """Handle mouse wheel event.

        :param float xPixel: X mouse position in pixels
        :param float yPixel: Y mouse position in pixels
        :param float angleInDegrees: Angle corresponding to wheel motion.
                                     Positive for movement away from the user,
                                     negative for movement toward the user.
        """
        if self._isPositionInPlotArea(xPixel, yPixel) == (xPixel, yPixel):
            self._eventHandler.handleEvent(
                'wheel', xPixel, yPixel, angleInDegrees)
            if MIDDLE_PAN:
                self._eventHandlerPan.handleEvent(
                    'wheel', xPixel, yPixel, angleInDegrees)

    def onMouseLeaveWidget(self):
        """Handle mouse leave widget event."""
        if self._cursorInPlot:
            self._cursorInPlot = False
            # for mouse-zoom action
            self._eventHandler.handleEvent('leave')

            if MIDDLE_PAN:
                # for mouse-pan action
                self._eventHandlerPan.handleEvent('leave')



def main():

    global app
    app = qt.QApplication([])

    # Create the ad hoc window containing a PlotWidget and associated tools
    window = PlotWidget()
    window.setAttribute(qt.Qt.WA_DeleteOnClose)
    window.show()

    app.exec_()



if __name__ == '__main__':
    main()
