import numpy as np
import logging
from pyFAI.azimuthalIntegrator import AzimuthalIntegrator
from xicam.core.execution.workflow import Workflow
from xicam.plugins import ProcessingPlugin, Input, Output

logger = logging.Logger(__name__)

class IntegratePlugin(ProcessingPlugin):
    """ Integrate process """
    args = Input(description='Integrate Parameters',
                 type=dict)
    ai = Input(description='AzimuthalIntegrator',
               type=AzimuthalIntegrator)
    result1d = Output(description='1-Dim Integration Result',
                      type=np.array)
    result2d = Output(description='2-Dim Integration Result',
                      type=np.array)

    def evaluate(self):
        Args = self.args.value
        ai = self.ai.value
        logger.debug("Integration Method = " + Args['method'])
        logger.debug("1D interation start")
        self.result1d.value = ai.integrate1d(data=Args['data'],
                                             mask=Args['mask'],
                                             npt=Args['npt_rad'],
                                             unit=Args['unit'],
                                             method=Args['method'],
                                             polarization_factor=Args['polarization_factor'])
        logger.debug("1D interation finished")

        logger.debug("2D interation start")
        self.result2d.value = ai.integrate2d(data=Args['data'],
                                             mask=Args['mask'],
                                             npt_rad=Args['npt_rad'],
                                             npt_azim=Args['npt_azim'],
                                             unit=Args['unit'],
                                             method=Args['method'],
                                             polarization_factor=Args['polarization_factor'])
        logger.debug("2D interation finished")


class IntegrateWorkflow(Workflow):
    """ Workflow is composed of series of plugins(process) """
    def __init__(self):
        super(IntegrateWorkflow, self).__init__('IntegrateWorkflow')

        process = IntegratePlugin()
        self.processes = [process]
        self.autoConnectAll()
