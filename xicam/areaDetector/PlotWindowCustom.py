import weakref
import logging

from silx.gui import qt
from silx.gui.plot.PlotWindow import PlotWindow
from silx.gui.plot.actions.control import ZoomBackAction, CrosshairAction,\
                                          ColormapAction
from silx.gui.plot.actions.io import SaveAction
from xicam.ptychography.ProfileCustom import ProfileToolBarCustom
from xicam.ptychography.fitCustom import FitActionCustom
from xicam.ptychography.DerivativeAction import DerivativeAction

logger = logging.getLogger(__name__)

class PlotWindowCustom(PlotWindow):

    # spectrum signal from ch1 to ch16
    sigSpectrumSelected = qt.Signal(object)

    def __init__(self, parent=None, backend=None,
                 resetzoom=True, autoScale=False,
                 logScale=False, grid=False,
                 curveStyle=False, colormap=True,
                 aspectRatio=False, yInverted=True,
                 copy=True, save=True, print_=True,
                 control=True, position=True,
                 roi=False, mask=False, fit=False):
        super(PlotWindowCustom, self).__init__(parent=parent, backend=backend,
                                               resetzoom=resetzoom,
                                               autoScale=autoScale,
                                               logScale=logScale,
                                               grid=grid,
                                               curveStyle=curveStyle,
                                               colormap=colormap,
                                               aspectRatio=aspectRatio,
                                               yInverted=yInverted,
                                               copy=copy,
                                               save=save,
                                               print_=print_,
                                               control=control,
                                               position=position,
                                               roi=roi, mask=mask, fit=fit)

        # Retrieve PlotWidget's plot area widget
        plotArea = self.getWidgetHandle()

        # Create QAction for the context menu once for all
        self._zoomBackAction = ZoomBackAction(plot=self, parent=self)
        self._crosshairAction = CrosshairAction(plot=self, parent=self)
        self._colormapAction = ColormapAction(plot=self, parent=self)

        # Set plot area custom context menu
        # plotArea.setContextMenuPolicy(qt.Qt.CustomContextMenu)
        # plotArea.customContextMenuRequested.connect(self._contextMenu)

        self.setGraphXLabel('')
        self.setGraphYLabel('')

        # ProfileToolBar
        profileToolBar = ProfileToolBarCustom(plot=self)
        self.addToolBar(profileToolBar)

        # Default colorbar on
        self.getColorBarWidget().setVisible(True)

        # Customized fitAction
        self.fitAction = self.group.addAction(FitActionCustom(self))
        self.fitAction.setVisible(fit)
        self.addAction(self.fitAction)

        # Invert Y-axis
        # self.setYAxisInverted(True)

    def moveMarkerToCenter(self):
        # Move marker to center
        center = self.getCenter()
        for marker in self._getAllMarkers():
            marker.setPosition(center[0], center[1])

    def setParent(self, parent):
        self._parent = parent

    def _contextMenu(self, pos):
        """Handle plot area customContextMenuRequested signal.

        :param QPoint pos: Mouse position relative to plot area
        """
        # Create the context menu
        menu = qt.QMenu(self)
        menu.addAction(self._zoomBackAction)
        menu.addSeparator()

        # Displaying the context menu at the mouse position requires
        # a global position.
        # The position received as argument is relative to PlotWidget's
        # plot area, and thus needs to be converted.
        plotArea = self.getWidgetHandle()
        globalPosition = plotArea.mapToGlobal(pos)
        menu.exec_(globalPosition)


    def graphCallback(self, ddict=None):
        """This callback is going to receive all the events from the plot.

        Those events will consist on a dictionary and among the dictionary
        keys the key 'event' is mandatory to describe the type of event.
        This default implementation only handles setting the active curve.
        """

        if ddict is None:
            ddict = {}
        if ddict['event'] in ["legendClicked", "curveClicked"]:
            if ddict['button'] == "left":
                self.setActiveCurve(ddict['label'])
                qt.QToolTip.showText(self.cursor().pos(), ddict['label'])

        if ddict['event'] in ['mouseMoved','MouseAt', 'mouseClicked']:
            # self._handleMouseMovedEvent(ddict)
            # print("ddict=", ddict)
            xPixel = ddict['xpixel']
            yPixel = ddict['ypixel']
            try:
                if self._isPositionInPlotArea(xPixel, yPixel) == (xPixel, yPixel):
                    data = self.getImage().getData()

                    dataPos = [ddict['x'], ddict['y']]
                    origin = self.getImage().getOrigin()
                    scale = self.getImage().getScale()

                    column = int((dataPos[0] - origin[0]) / float(scale[0]))
                    row = int((dataPos[1] - origin[1]) / float(scale[1]))

                    if scale[0] is not None:
                        x = origin[0] + column * scale[0]
                        y = origin[1] + row * scale[1]
                    else:
                        x = column
                        y = row

                if column < data.shape[1] and row < data.shape[0]:
                    self.setMouseText("%g, %g, %g" % (x, y, data[row, column]))

                # Update energy-spectrum with mouse click
                # if ddict['event'] == 'mouseClicked':
                #     pos = {'x' : column, 'y' : row, 'xData' : x, 'yData' : y}
                #     self.sigSpectrumSelected.emit(pos)
            except:
                pass
                # print("Error on displaying data tooltip")

        self.sigPlotSignal.emit(ddict)


    def setMouseText(self, text=""):
        try:
            if len(text):
                qt.QToolTip.showText(self.cursor().pos(),
                                     text, self, qt.QRect())
            else:
                qt.QToolTip.hideText()
        except:
            logger.warning("Error trying to show mouse text <%s>" % text)



class Plot1DCustom(PlotWindow):
    """PlotWindow with tools specific for curves.

    This widgets provides the plot API of :class:`.PlotWidget`.

    :param parent: The parent of this widget
    :param backend: The backend to use for the plot (default: matplotlib).
                    See :class:`.PlotWidget` for the list of supported backend.
    :type backend: str or :class:`BackendBase.BackendBase`
    """

    def __init__(self, parent=None, backend=None):
        super(Plot1DCustom, self).__init__(parent=parent, backend=backend,
                                           resetzoom=True, autoScale=True,
                                           logScale=True, grid=True,
                                           curveStyle=True, colormap=False,
                                           aspectRatio=False, yInverted=False,
                                           copy=True, save=True, print_=True,
                                           control=True, position=True,
                                           roi=False, mask=False, fit=True)
        self._parentRef = weakref.ref(parent)

        if parent is None:
            self.setWindowTitle('Plot1D')
        self.getXAxis().setLabel('X')
        self.getYAxis().setLabel('Y')

        # Customized fitAction
        # remove previous toolbar
        self.removeToolBar(self._toolbar)
        self.group.removeAction(self.fitAction)

        # create a new fit action
        self.fitAction = self.group.addAction(FitActionCustom(self))
        self.fitAction.setVisible(True)
        self.addAction(self.fitAction)

        # create a new derivative action
        self.derivativeAction = self.group.addAction(DerivativeAction(self))
        self.derivativeAction.setVisible(True)
        self.addAction(self.derivativeAction)

        # create toolbar
        self._toolbar = self._createToolBar(title='Plot', parent=None)
        self.addToolBar(self._toolbar)


    @property
    def parent(self):
        return self._parentRef()

    def hideMarkers(self):
        markers = self.parent._getAllMarkers()
        for marker in markers:
            marker.setVisible(False)

    def getMarker(self, legend):
        """Get the marker with the name of legend
        """
        return self._getMarker(legend)

def main():

    global app
    app = qt.QApplication([])

    # Create the ad hoc window containing a PlotWidget and associated tools
    window = PlotWindowCustom()
    window.setAttribute(qt.Qt.WA_DeleteOnClose)
    window.show()

    app.exec_()



if __name__ == '__main__':
    main()
