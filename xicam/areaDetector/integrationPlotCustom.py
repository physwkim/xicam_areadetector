from silx.gui import qt
import logging
from pyFAI import units
from pyFAI.gui.tasks.IntegrationTask import IntegrationPlot as _IntegrationPlot

logger = logging.getLogger(__name__)

class IntegrationPlot(_IntegrationPlot):
    def __init__(self, parent=None):
        super(IntegrationPlot, self).__init__(parent)

    def getMarkerManager(self):
        return self.__markerManager

    def setHistogram(self, result1d):
        self.__plot1d.addHistogram(legend="result1d",
                                   align="center",
                                   edges=result1d[0],
                                   color="blue",
                                   histogram=result1d[1],
                                   resetzoom=False)

        # Enable save 1d graph action
        self.__setResult(result1d)

    def __plot2dContextMenu(self, pos):
        from silx.gui.plot.actions.control import ZoomBackAction
        zoomBackAction = ZoomBackAction(plot=self.__plot2d, parent=self.__plot2d)

        menu = qt.QMenu(self)

        menu.addAction(zoomBackAction)

        """
        menu.addSeparator()
        menu.addAction(self.__markerManager.createMarkPixelAction(menu, pos))
        menu.addAction(self.__markerManager.createMarkGeometryAction(menu, pos))
        action = self.__markerManager.createRemoveClosestMaskerAction(menu, pos)
        if action is not None:
            menu.addAction(action)
        """
        handle = self.__plot2d.getWidgetHandle()
        menu.exec_(handle.mapToGlobal(pos))

    def setImage(self, result2d):
        # result2d : intensity, radial, azimuthal
        intensity = result2d[0]
        radial = result2d[1]
        azimuthal = result2d[2]

        if intensity.shape[1] > 1:
            scaleX = (radial[-1] - radial[0]) / (intensity.shape[1] - 1)
        else:
            scaleX = 1.0

        if intensity.shape[0] > 1:
            scaleY = (azimuthal[-1] - azimuthal[0]) / (intensity.shape[0] - 1)
        else:
            scaleY = 1.0

        halfPixel = 0.5 * scaleX, 0.5 * scaleY
        origin = (radial[0] - halfPixel[0], azimuthal[0] - halfPixel[1])

        colormap = self.getDefaultColormap()
        self.__plot2d.addImage(legend="integrated_data",
                               data=intensity,
                               origin=origin,
                               scale=(scaleX, scaleY),
                               colormap=colormap,
                               resetzoom=False)

        # Rescale self.__plot1d & self.__plot2d
        self.resetZoom()

    def setRadialUnit(self, radialUnit):
        self.__radialUnit = units.RADIAL_UNITS[radialUnit]

    def setWavelength(self, wavelength):
        self.__wavelength = wavelength

    def setDirectDist(self, directDist):
        self.__directDist = directDist

    def setGeometry(self, geometry):
        self.__geometry = geometry

    def setInverseGeometry(self, invertGeometry):
        self.__inverseGeometry = invertGeometry
